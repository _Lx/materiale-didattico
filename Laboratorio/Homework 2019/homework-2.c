/*
    Homework n.2

    Estendere l'esempio 'move.c' visto a lezione per supportare i 2 casi speciali:
    - spostamento cross-filesystem: individuato tale caso, il file deve essere
     spostato utilizzando la strategia "copia & cancella";
    - spostamento di un link simbolico: individuato tale caso, il link simbolico
     deve essere ricreato a destinazione con lo stesso contenuto (ovvero il percorso
     che denota l'oggetto referenziato); notate come tale spostamento potrebbe
     rendere il nuovo link simbolico non effettivamente valido.

    La sintassi da supportare e' la seguente:
     $ homework-2 <pathname sorgente> <pathname destinazione>
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <libgen.h>

#define BUFFER_SIZE 2048

char* getDest(char* input, char* destfolder){
    char* namefile = basename(input);
    char* dest = (char*) malloc(BUFFER_SIZE);
    strcpy(dest, destfolder);
    if(dest[strlen(dest)-1] != '/')
        dest = strcat(dest, "/");

    dest = strcat(dest, namefile);

    return dest;
}

int main(int argc, char* argv[]){
    if(argc < 3){
        printf("USE:%s <file-esistente> <nuovo-nome-e-o-posizione>.\n", argv[0]);
        exit(1);
    }

    struct stat buffer1;
    struct stat buffer2;

    lstat(argv[1], &buffer1);
    lstat(argv[2], &buffer2);
    char* dest = getDest(argv[1], argv[2]);

    if((buffer1.st_mode & S_IFMT) == S_IFLNK){
        char bufferlink[BUFFER_SIZE];
        char actualpath[BUFFER_SIZE+1];
        int size;

        if((size = readlink(argv[1], bufferlink, BUFFER_SIZE)) == -1){
            perror(argv[1]);
            exit(1);
        }

        bufferlink[size] = '\0';

        char* tmp = realpath(argv[1], actualpath);

        if(symlink(tmp, dest) == -1){
            perror(dest);
            exit(1);
        }

    }
    else{
        if(buffer1.st_dev == buffer2.st_dev){
            if(link(argv[1], dest) == -1){
                perror(dest);
                exit(1);
            }

            if(unlink(argv[1]) == -1){
                perror(argv[1]);
                exit(1);
            }
        }
        else{
            FILE* input = fopen(argv[1], "r");
            FILE* output = fopen(dest, "w");

            char tmp;
            while((tmp = fgetc(input)) != EOF){
                fputc(tmp, output);
            }

            fclose(input);
            fclose(output);

            if(unlink(argv[1]) == -1){
                perror(argv[1]);
                exit(1);
            }
        }
    }
    free(dest);

}
