/*
    Homework n.4

    Estendere l'esercizio 'homework n.1' affinche' operi correttamente
    anche nel caso in cui tra le sorgenti e' indicata una directory, copiandone
    il contenuto ricorsivamente. Eventuali link simbolici incontrati dovranno
    essere replicati come tali (dovrà essere creato un link e si dovranno
    preservare tutti permessi di accesso originali dei file e directory).

    Una ipotetica invocazione potrebbe essere la seguente:
     $ homework-4 directory-di-esempio file-semplice.txt path/altra-dir/ "nome con spazi.pdf" directory-destinazione
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>

#define BUFFSIZE 2048


void copyLNK(char* input, char* output);
void copyFolder(char* input, char* output, struct stat* st);
void copyFile(char* input, char* output);
void checkType(char* input, char* output);

int main(int argc, char* argv[]){
    if(argc < 3){
        printf("Use %s <dir/file> <output>\n", argv[0]);
        exit(1);
    }

    for(int i = 1; i < argc - 1; i++)
        checkType(argv[i], argv[argc - 1]);
}

void checkType(char* input, char* output){
    struct stat inputBuffer;
    lstat(input, &inputBuffer);

    switch(inputBuffer.st_mode & S_IFMT){
            case S_IFLNK:       //Caso link simbolico
                copyLNK(input, output);
            break;

            case S_IFDIR:       //Caso cartella
                copyFolder(input, output, &inputBuffer);
            break;

            case S_IFREG:       //Caso file regolare
                copyFile(input, output);
            break;

            default:
            printf("Tipo file non supportato\n");
            exit(1);
    }
}

char* getDest(char* input, char* destfolder){
    char* namefile = basename(input);
    char* dest = (char*) malloc(BUFFSIZE);
    strcpy(dest, destfolder);
    if(dest[strlen(dest)-1] != '/')
        dest = strcat(dest, "/");

    dest = strcat(dest, namefile);

    return dest;
}

void copyLNK(char* input, char* output){
    char bufferlink[BUFFSIZE];
    char actualpath[BUFFSIZE+1];
    int size;

    if((size = readlink(input, bufferlink, BUFFSIZE)) == -1){
        perror(input);
        exit(1);
    }

    bufferlink[size] = '\0';
    char* dest = getDest(input, output);
    char* tmp = realpath(input, actualpath);

    if(symlink(tmp, dest) == -1){
        perror(dest);
        exit(1);
    }

    free(dest);
}


void copyFolder(char* input, char* output, struct stat* st){
    DIR* fd;
    struct dirent* entry;
    char* pathname = getDest(input, output);

    
    if(mkdir(pathname, st->st_mode & 0777) == -1){
        perror(pathname);
        exit(1);
    }

    if((fd = opendir(input)) == NULL){
        perror(input);
        exit(1);
    }

    if(pathname[strlen(pathname) -1] != '/')
        strcat(pathname, "/");
    
    if(input[strlen(input) - 1] != '/')
        strcat(input, "/");

    char tmp[BUFFSIZE];
    
    while((entry = readdir(fd)) != NULL){
        if(strcmp(entry->d_name, "..") != 0 && strcmp(entry->d_name, ".") != 0){
            strcpy(tmp, input);
            strcat(tmp, entry->d_name);
            printf("File: %s Output: %s\n", tmp, pathname);
            copyFile(tmp, pathname);
        }
    }

    free(entry);
    free(fd);
    free(pathname);
}

void copyFile(char* input, char* output){
    FILE* finput;
    FILE* foutput;

    finput = fopen(input, "r");

    char* dest = getDest(input, output);

    foutput = fopen(dest, "w+");

    char tmp;
    while((tmp = fgetc(finput)) != EOF){
        fputc(tmp, foutput);
    }

    fclose(finput);
    fclose(foutput);
    free(dest);
}