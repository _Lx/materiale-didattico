#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

typedef struct {
    long mtype;
    char line[1024];
    int stop;
} rmsg_t;

typedef struct {
    long mtype;
    int stats[26];
    int stop;
} cmsg_t;

void r(int msgqid, const char *file) {
    rmsg_t msg = {.mtype = 1, .stop = 0};
    size_t msgSz = sizeof(rmsg_t) - sizeof(long);
    FILE *fd = fopen(file, "r");
    char line[1024] = {0};

    if (fd == NULL) {
        printf("failed to open %s\n", file);
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        snprintf(msg.line, sizeof(msg.line), "%s", line);

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send r\n");
    }

    fclose(fd);

    msg.stop = 1;
    if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
        printf("failed to send stop r\n");
}

void counter(int rmsgqid, int pmsgqid, int readersC) {
    cmsg_t cmsg = {.mtype = 1, .stop = 0};
    size_t rmsgSz = sizeof(rmsg_t) - sizeof(long);
    size_t cmsgSz = sizeof(cmsg_t) - sizeof(long);
    struct msqid_ds msgds;
    rmsg_t rmsg;

    while (1) {
        if (msgctl(rmsgqid, IPC_STAT, &msgds) == -1)
            printf("failed to get rmsg que stats");

        if (readersC == 0 && msgds.msg_qnum == 0)
            break;

        if (msgrcv(rmsgqid, &rmsg, rmsgSz, 1, 0) == -1)
            printf("failed to rcv counter\n");

        if (rmsg.stop) {
            --readersC;
            if (readersC == 0) break;
            continue;
        }

        for (int i = 0, l = strlen(rmsg.line); i < l; ++i) {
            int ascii = (int) rmsg.line[i];
            int idx = ascii < 97 ? ascii + 32 - 97 : ascii - 97;

            if (idx < 0 || idx > 122)
                continue;

            ++cmsg.stats[idx];
        }

        if (msgsnd(pmsgqid, &cmsg, cmsgSz, 0) == -1)
            printf("failed to send counter\n");

        memset(cmsg.stats, 0, sizeof(cmsg.stats));
    }

    cmsg.stop = 1;
    if (msgsnd(pmsgqid, &cmsg, cmsgSz, 0) == -1)
        printf("failed to send stop counter\n");
}

pid_t startCounter(int rmsgqid, int pmsgqid, int readersC) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    counter(rmsgqid, pmsgqid, readersC);
    exit(0);
}

void printStats(int gstats[]) {
    char out[512] = {0};
    char buf[10] = {0};

    for (int i = 0; i < 26; ++i) {
        snprintf(buf, sizeof(buf), "%c:%d ", i + 97, gstats[i]);
        strncat(out, buf, strlen(buf));
    }

    printf("%s\n", out);
}

int main(int argc, char *argv[]) {
    size_t cmsgSz = sizeof(cmsg_t) - sizeof(long);
    int gstats[26] = {0};
    int isChild = 0;
    int rmsgqid, cmsgqid;
    cmsg_t cmsg;
    int i;

    if (argc < 2) {
        printf("usage: alphabet-stats <file-1> <file-2> ... <file-n>\n");
        return 1;
    }

    rmsgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (rmsgqid == -1) {
        printf("failed to create msg r que\n");
        return 1;
    }

    cmsgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (cmsgqid == -1) {
        printf("failed to create msg c que\n");
        msgctl(rmsgqid, IPC_RMID, NULL);
        return 1;
    }

    if (startCounter(rmsgqid, cmsgqid, argc - 1) == -1) {
        printf("failed to start counter\n");
        msgctl(rmsgqid, IPC_RMID, NULL);
        msgctl(cmsgqid, IPC_RMID, NULL);
        return 1;
    }

    for (i = 1; i < argc; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (child == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        r(rmsgqid, argv[i]);
        return 0;
    }

    while (1) {
        if (msgrcv(cmsgqid, &cmsg, cmsgSz, 1, 0) == -1)
            printf("failed to rcv parent\n");

        if (cmsg.stop)
            break;

        for (int i = 0; i < 26; ++i)
            gstats[i] += cmsg.stats[i];
    }

    printStats(gstats);
    msgctl(rmsgqid, IPC_RMID, NULL);
    msgctl(cmsgqid, IPC_RMID, NULL);
    return 0;
}
