#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/msg.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

typedef struct {
    long mtype;
    char cmd;
    char fname[NAME_MAX];
    char searchChar;
    int res;
    int stop;
} msg_t;

void d(const char *path, int msgqid, int id, int msgResType) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    msg_t msg;

    while (1) {
        int c = 0;

        if (msgrcv(msgqid, &msg, msgSz, id, 0) == -1) {
            printf("failed to rcv d %d\n", id);
            continue;
        }

        if (msg.stop == 1)
            break;

        switch (msg.cmd) {
            case 'n':
            case 't': {
                DIR *dfd = opendir(path);
                struct dirent *ds;

                if (dfd == NULL) {
                    printf("failed to open dir %s\n", path);
                    break;
                }

                while (1) {
                    ds = readdir(dfd);

                    if (ds == NULL)
                        break;
                    else if (ds->d_type != DT_REG)
                        continue;

                    if (msg.cmd == 'n') {
                        ++c;

                    } else {
                        char buf[PATH_MAX] = {0};
                        struct stat st;

                        snprintf(buf, sizeof(buf), "%s/%s", path, ds->d_name);
                        stat(buf, &st);

                        c += st.st_size;
                    }
                }

                closedir(dfd);
            }
                break;
            case 's': {
                char buf[PATH_MAX] = {0};
                struct stat st;
                char *fmap;
                int fd;

                snprintf(buf, sizeof(buf), "%s/%s", path, msg.fname);
                stat(buf, &st);

                fd = open(buf, O_RDONLY);
                if (fd == -1) {
                    printf("failed to open file %s\n", buf);
                    break;
                }

                fmap = (char *) mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
                if (fmap == MAP_FAILED) {
                    printf("mmap fail %s\n", buf);
                    close(fd);
                    break;
                }

                close(fd);

                for (int i = 0, l = st.st_size; i < l; ++i) {
                    if (fmap[i] == msg.searchChar)
                        ++c;
                }

                munmap(fmap, st.st_size);
            }
                break;
            default:
                break;
        }

        msg.mtype = msgResType;
        msg.res = c;

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send res %d\n", id);
    }
}

int main(int argc, char *argv[]) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    char buf[200] = {0};
    int isChild = 0;
    msg_t msg = {0};
    pid_t *children;
    int msgResType;
    int msgqid;
    int pnum;
    int i;

    if (argc < 2) {
        printf("usage: file-shell2 <directory-1> <directory-2> <...>\n");
        return 1;
    }

    pnum = argc - 1;
    msgResType = argc + 1;

    children = (pid_t *) malloc(sizeof(pid_t) * pnum);
    if (children == NULL) {
        printf("failed to alloc children\n");
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msg que\n");
        free(children);
        return 1;
    }

    for (i = 1; i < argc; ++i) {
        int j = i - 1;

        children[j] = fork();

        if (children[j] == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (children[j] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        size_t slen = strlen(argv[i]) - 1;

        if (argv[i][slen] == '/')
            argv[i][slen] = '\0';

        d(argv[i], msgqid, i, msgResType);
        return 0;
    }

    printf("file-shell> ");

    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        char fname[NAME_MAX] = {0};
        char cmd[20] = {0};
        char searchChar;
        int procID;
        int ret;

        if (strncmp(buf, "qq", strlen("qq")) == 0)
            break;

        ret = sscanf(buf, "%s %d %s %c", cmd, &procID, fname, &searchChar);

        msg.mtype = procID;
        msg.cmd = cmd[0];

        if (ret > 2) {
            snprintf(msg.fname, sizeof(msg.fname), "%s", fname);
            msg.searchChar = searchChar;
        }

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1) {
            printf("failed to send cmd %s\n\n", cmd);
            printf("file-shell> ");
            continue;
        }

        if (msgrcv(msgqid, &msg, msgSz, msgResType, 0) == -1) {
            printf("failed to rcv res\n\n");
            printf("file-shell> ");
            continue;
        }

        if (cmd[0] == 'n')
            printf("%d files\n\n", msg.res);
        else if (cmd[0] == 't')
            printf("%d bytes\n\n", msg.res);
        else
            printf("%d\n\n", msg.res);

        printf("file-shell> ");
    }

    for (int i = 1; i < argc; ++i) {
        msg.mtype = i;
        msg.stop = 1;

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send stop p\n");
    }

    for (int i = 0; i < pnum; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    msgctl(msgqid, IPC_RMID, NULL);
    free(children);
    return 0;
}
