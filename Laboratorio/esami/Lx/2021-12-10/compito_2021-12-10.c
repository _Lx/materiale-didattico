#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/wait.h>

#define LINE_READY_SEM 0
#define LINE_PARSED_SEM 1

#define SEMAOP(semid, sbuf, semnum, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

typedef struct {
    long mtype;
    char lChar;
    int count;
} msg_t;

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

void l(int semid, int shmid, int msgqid, char ownChar) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    msg_t msg = {.mtype = 1, .lChar = ownChar};
    struct sembuf sbuf = {0};
    char *shmem;

    shmem = (char *) shmat(shmid, NULL, SHM_RDONLY);
    if (shmem == (void *) -1) {
        printf("failed to attach mem child\n");
        return;
    }

    while (1) {
        int i = 0;

        SEMAOP(semid, sbuf, LINE_READY_SEM, -1)

        if (strncmp(shmem, "qq", strlen("qq")) == 0)
            break;

        msg.count = 0;
        while (shmem[i] != '\n') {
            if (tolower((unsigned char) shmem[i]) == ownChar)
                ++msg.count;

            ++i;
        }

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("%c failed to send msg\n", ownChar);
    }

    msg.count = -1;
    if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
        printf("%c failed to send stop msg\n", ownChar);
}

char *getStatsStr(int stats[]) {
    size_t rsz = sizeof(char) * 200;
    char *ret = (char *) malloc(rsz);

    if (ret == NULL)
        return NULL;

    memset(ret, 0, rsz);

    for (int i = 0; i < 26; ++i) {
        char buf[20] = {0};

        snprintf(buf, sizeof(buf), "%c:%d ", i + 97, stats[i]);
        strncat(ret, buf, strlen(buf));
    }

    return ret;
}

void s(int semid, int msgqid) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    struct sembuf sbuf = {0};
    int gstats[26] = {0};
    int stats[26] = {0};
    int lineNo = 1;
    int rcvC = 0;
    int qqC = 0;
    char *statsStr;
    msg_t msg;

    while (1) {
        rcvC = 0;

        while (rcvC < 26 && qqC < 26) {
            int ret = msgrcv(msgqid, &msg, msgSz, 1, 0);
            int idx;

            if (ret == -1) {
                printf("failed to rcv\n");
                continue;

            } else if (msg.count == -1) {
                ++qqC;
                continue;
            }

            idx = (int) msg.lChar - 97;
            if (idx < 0 || idx >= 26) {
                printf("invalid idx %d for %c\n", idx, msg.lChar);
                continue;
            }

            stats[idx] = msg.count;

            ++rcvC;
        }

        if (qqC >= 26)
            break;

        for (int i = 0; i < 26; ++i)
            gstats[i] += stats[i];

        statsStr = getStatsStr(stats);

        printf("[S] riga n.%d: %s\n", lineNo++, statsStr);
        memset(stats, 0, sizeof(stats));
        free(statsStr);
        SEMAOP(semid, sbuf, LINE_PARSED_SEM, 1);
    }

    statsStr = getStatsStr(gstats);

    printf("[S] intero file: %s\n", statsStr);
    free(statsStr);
}

pid_t startS(int semid, int msgqid) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    s(semid, msgqid);
    exit(0);
}

void p(int semid, const char *file, char *shmem) {
    struct sembuf sbuf = {0};
    FILE *fd = fopen(file, "r");
    char buf[2048] = {0};
    int lineNo = 1;

    if (fd == NULL) {
        printf("failed to open file\n");
        return;
    }

    while (fgets(buf, sizeof(buf), fd) != NULL) {
        printf("[P]  riga   n.%d: %s", lineNo++, buf);
        strncpy(shmem, buf, strlen(buf));
        SEMAOP(semid, sbuf, LINE_READY_SEM, 26)
        SEMAOP(semid, sbuf, LINE_PARSED_SEM, -1)
    }

    fclose(fd);
    strncpy(shmem, "qq", strlen("qq"));
    SEMAOP(semid, sbuf, LINE_READY_SEM, 26)
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    int ownCh = 97;
    int semid, msgqid, shmid;
    pid_t children[27];
    union semun sarg;
    char *shmem;
    int i;

    if (argc < 2) {
        printf("parallel-alpha-stats <text-file>");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 2, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msq que\n");
        semctl(semid, 0, IPC_RMID);
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(char) * 2048, IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        semctl(semid, 0, IPC_RMID);
        msgctl(msgqid, IPC_RMID, NULL);
        return 1;
    }

    shmem = (char *) shmat(shmid, NULL, 0);
    if (shmem == (void *) -1) {
        printf("failed to attach mem\n");
        semctl(semid, 0, IPC_RMID);
        msgctl(msgqid, IPC_RMID, NULL);
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    sarg.val = 0;

    semctl(semid, LINE_READY_SEM, SETVAL, sarg);
    semctl(semid, LINE_PARSED_SEM, SETVAL, sarg);

    children[0] = startS(semid, msgqid);
    if (children[0] == -1) {
        printf("failed to start s\n");
        semctl(semid, 0, IPC_RMID);
        msgctl(msgqid, IPC_RMID, NULL);
        shmctl(shmid, IPC_RMID, NULL);
        return -1;
    }

    for (i = 1; i < 27; ++i) {
        children[i] = fork();

        if (children[i] == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (children[i] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        l(semid, shmid, msgqid, (char) (ownCh + i - 1));
        return 0;
    }

    p(semid, argv[1], shmem);

    for (int i = 0; i < 27; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    semctl(semid, 0, IPC_RMID);
    msgctl(msgqid, IPC_RMID, NULL);
    shmctl(shmid, IPC_RMID, NULL);
    return 0;
}
