#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define WAIT(semid, sbuf, semnum) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = -1; \
    semop(semid, &sbuf, 1); \
}

#define SIGNAL(semid, sbuf, semnum) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = 1; \
    semop(semid, &sbuf, 1); \
}

// non sono sicuro siano necessari 3 semafori per calculator,
// ma dalla richiesta del compito, non vedo altro modo?
#define OP_READY_SEM 0
#define ADD_OP_SEM 1
#define SUB_OP_SEM 2
#define MUL_OP_SEM 3

typedef struct {
    long val;
    long opr;
    short done;
} sharedData_t;

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

void calculator(int shmid, int semid, char op) {
    sharedData_t *shmd = (sharedData_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};
    int hasWork = 1;

    if (shmd == (void *) -1) {
        printf("calproc %c: failed to attach shared mem\n", op);
        return;
    }

    while (hasWork) {
        switch (op) {
            case '+': {
                WAIT(semid, sbuf, ADD_OP_SEM)

                if (shmd->done) {
                    hasWork = 0;
                    break;
                }

                long old = shmd->val;

                shmd->val += shmd->opr;
                printf("ADD: %ld+%ld=%ld\n", old, shmd->opr, shmd->val);
            }
                break;
            case '-': {
                WAIT(semid, sbuf, SUB_OP_SEM)

                if (shmd->done) {
                    hasWork = 0;
                    break;
                }

                long old = shmd->val;

                shmd->val -= shmd->opr;
                printf("SUB: %ld-%ld=%ld\n", old, shmd->opr, shmd->val);
            }
                break;
            case '*': {
                WAIT(semid, sbuf, MUL_OP_SEM)

                if (shmd->done) {
                    hasWork = 0;
                    break;
                }

                long old = shmd->val;

                shmd->val *= shmd->opr;
                printf("MUL: %ld*%ld=%ld\n", old, shmd->opr, shmd->val);
            }
                break;
            default: {
                printf("wth?! child op invalid\n");
                return;
            }
        }

        SIGNAL(semid, sbuf, OP_READY_SEM)
    }

}

void mng(int shmid, int semid, const char *file) {
    sharedData_t *shmd = (sharedData_t *) shmat(shmid, NULL, 0);
    FILE *fd = fopen(file, "r");
    struct sembuf sbuf = {0};
    char line[50] = {0};

    if (fd == NULL) {
        printf("failed to open file\n");
        return;
    }

    if (shmd == (void *) -1) {
        printf("mng: failed to attach shared mem\n");
        fclose(fd);
        return;
    }

    shmd->val = 0;
    shmd->done = 0;

    while (fgets(line, sizeof(line), fd) != NULL) {
        size_t slen = strlen(line);
        char op = line[0];

        if (line[slen - 1] == '\n')
            line[slen - 1] = '\0';

        WAIT(semid, sbuf, OP_READY_SEM)

        printf("MNG: risultato intermedio: %ld; letto \"%s\"\n", shmd->val, line);

        line[0] = '0'; // rimuovi simbolo per atol
        shmd->opr = atol(line);

        switch (op) {
            case '+': {
                SIGNAL(semid, sbuf, ADD_OP_SEM)
            }
                break;
            case '-': {
                SIGNAL(semid, sbuf, SUB_OP_SEM)
            }
                break;
            case '*': {
                SIGNAL(semid, sbuf, MUL_OP_SEM)
            }
                break;
            default:
                printf("mng: unknown op\n");
                break;
        }
    }

    WAIT(semid, sbuf, OP_READY_SEM)
    printf("MNG: risultato finale: %ld\n", shmd->val);

    shmd->done = 1;

    SIGNAL(semid, sbuf, ADD_OP_SEM)
    SIGNAL(semid, sbuf, SUB_OP_SEM)
    SIGNAL(semid, sbuf, MUL_OP_SEM)

    fclose(fd);
}

pid_t startMngProc(int shmid, int semid, const char *file) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    mng(shmid, semid, file);
    exit(0);
}

int main(int argc, char *argv[]) {
    const char ops[] = {'+', '-', '*'};
    int isChild = 0;
    pid_t children[4];
    union semun sarg;
    int shmid;
    int semid;
    int i;

    if (argc != 2) {
        printf("usage: calculator <list.txt>\n");
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(sharedData_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 4, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create semaphores\n");
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    children[0] = startMngProc(shmid, semid, argv[1]);
    if (children[0] == -1) {
        printf("failed to start mng proc\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        return 1;
    }

    sarg.val = 1;
    semctl(semid, OP_READY_SEM, SETVAL, sarg);

    sarg.val = 0;
    semctl(semid, ADD_OP_SEM, SETVAL, sarg);
    semctl(semid, SUB_OP_SEM, SETVAL, sarg);
    semctl(semid, MUL_OP_SEM, SETVAL, sarg);

    for (i = 1; i < 4; ++i) {
        children[i] = fork();

        if (children[i] == -1) {
            printf("failed to create child %d\n", i);

        } else if (children[i] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        calculator(shmid, semid, ops[i - 1]);
        return 0;
    }

    for (i = 0; i < 4; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
