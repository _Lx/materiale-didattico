#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/msg.h>
#include <sys/wait.h>

typedef struct {
    long mtype;
    int pNum;
    int pID;
    int stop;
} msg_t;

void p(int msgqid, int id, int msgPType) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    msg_t msg;

    while (1) {
        if (msgrcv(msgqid, &msg, msgSz, id + 1, 0) == -1)
            printf("failed to rcv %d\n", id);

        if (msg.stop == 1)
            break;

        msg.mtype = msgPType;
        msg.pID = id;
        msg.pNum = rand() % 10;

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send %d\n", id);
    }

}

int isTie(const int *stats, int len) {
    for (int i = 0; i < len; ++i) {
        for (int j = i + 1; j < len; ++j) {
            if (stats[i] == stats[j])
                return 1;
        }
    }

    return 0;
}

int getWinn(const int *stats, int len) {
    int ret = 0;

    for (int i = 0; i < len; ++i)
        ret += stats[i];

    return (ret % len);
}

void printStats(const int *gstats, int len) {
    char ret[100] = {0};

    for (int i = 0; i < len; ++i) {
        char buf[20] = {0};

        snprintf(buf, sizeof(buf), "P%d=%d ", i, gstats[i]);
        strncat(ret, buf, strlen(buf));
    }

    printf("J: classifica finale: %s\n", ret);
}

int getGWinn(const int *gstats, int len) {
    int w = 0;

    for (int i = 0; i < len; ++i) {
        if (gstats[i] > w)
            w = i;
    }

    return w;
}

int main(int argc, char *argv[]) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    int isChild = 0;
    pid_t *children;
    int msgPType;
    int *gstats;
    int *stats;
    int msgqid;
    msg_t msg;
    int pnum;
    int gnum;
    int i;

    if (argc < 3) {
        printf("usage: pari-dispari-generalizzato <n=numero-giocatori> <m=numero-partite>\n");
        return 1;
    }

    pnum = atoi(argv[1]);
    gnum = atoi(argv[2]);
    msgPType = pnum + 1;

    children = (pid_t *) malloc(sizeof(pid_t) * pnum);
    if (children == NULL) {
        printf("failed to alloc children\n");
        return 1;
    }

    stats = (int *) calloc(pnum, sizeof(int));
    if (stats == NULL) {
        printf("failed to alloc stats\n");
        free(children);
        return 1;
    }

    gstats = (int *) calloc(pnum, sizeof(int));
    if (stats == NULL) {
        printf("failed to alloc gstats\n");
        free(stats);
        free(children);
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msg que\n");
        free(stats);
        free(gstats);
        free(children);
        return 1;
    }

    for (i = 0; i < pnum; ++i) {
        children[i] = fork();

        if (children[i] == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (children[i] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        srand(time(NULL) * (i + 1) * 1000);
        p(msgqid, i, msgPType);
        return 0;
    }

    msg.stop = 0;

    for (int i = 0; i < gnum; ++i) {
        int matchNo = i + 1;
        int winn;

        printf("J:  inizio partita n.%d\n", matchNo);

        for (int j = 0; j < pnum; ++j) { // chiedi di fare una mossa a tutti
            msg.mtype = j + 1;

            if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
                printf("failed to send to %d\n", j);
        }

        for (int j = 0; j < pnum; ++j) { // registra le mosse
            if (msgrcv(msgqid, &msg, msgSz, msgPType, 0) == -1)
                printf("failed to rcv p\n");

            stats[msg.pID] = msg.pNum;
            printf("P%d: mossa %d\n", msg.pID, msg.pNum);
        }

        if (isTie(stats, pnum)) {
            printf("J:  partita n.%d patta e quindi da ripetere\n", matchNo);
            --i;
            continue;
        }

        winn = getWinn(stats, pnum);
        ++gstats[winn];

        printf("J:  partita n.%d vinta da P%d\n", matchNo, winn);
    }

    for (int i = 0; i < pnum; ++i) { // invia stop a tutti
        msg.mtype = i + 1;
        msg.stop = 1;

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send stop %d\n", i);
    }

    for (int i = 0; i < pnum; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    printStats(gstats, pnum);
    printf("J: vincitore del torneo: P%d\n", getGWinn(gstats, pnum));

    msgctl(msgqid, IPC_RMID, NULL);
    free(stats);
    free(gstats);
    free(children);
    return 0;
}
