#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/msg.h>
#include <sys/wait.h>

#define MSG_COUNTER_TYPE 1
#define MSG_READER_TYPE 1

typedef struct {
    long mtype;
    int stats[26];
    int id;
} cmsg_t;

typedef struct {
    long mtype;
    char line[1024];
    int id;
} rmsg_t;

void r(int cmsgqid, int pmsgqid, int id, const char *file) {
    rmsg_t rmsg = {.mtype = MSG_COUNTER_TYPE, .id = id};
    size_t rmsgSz = sizeof(rmsg_t) - sizeof(long);
    size_t cmsgSz = sizeof(cmsg_t) - sizeof(long);
    FILE *fd = fopen(file, "r");
    char line[1024] = {0};
    int gstats[26] = {0};
    cmsg_t cmsg;

    if (fd == NULL) {
        printf("failed to open %s\n", file);
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        snprintf(rmsg.line, sizeof(rmsg.line), "%s", line);

        if (msgsnd(cmsgqid, &rmsg, rmsgSz, 0) == -1) {
            printf("failed to send %d\n", id);
            continue;
        }

        if (msgrcv(cmsgqid, &cmsg, cmsgSz, id, 0) == -1) {
            printf("failed to rcv %d\n", id);
            continue;
        }

        for (int i = 0; i < 26; ++i)
            gstats[i] += cmsg.stats[i];
    }

    fclose(fd);

    memcpy(cmsg.stats, gstats, sizeof(gstats));
    cmsg.mtype = MSG_READER_TYPE;
    cmsg.id = id;

    if (msgsnd(pmsgqid, &cmsg, cmsgSz, 0) == -1)
        printf("failed to send stats %d\n", id);
}

void counter(int msgqid) {
    size_t cmsgSz = sizeof(cmsg_t) - sizeof(long);
    size_t rmsgSz = sizeof(rmsg_t) - sizeof(long);
    cmsg_t cmsg;
    rmsg_t rmsg;

    while (1) {
        if (msgrcv(msgqid, &rmsg, rmsgSz, MSG_COUNTER_TYPE, 0) == -1) {
            printf("failed to rcv counter\n");
            continue;
        }

        if (rmsg.id == -1)
            break;

        memset(cmsg.stats, 0, sizeof(cmsg.stats));

        for (int i = 0, l = strlen(rmsg.line); i < l; ++i) {
            int ascii = (int) rmsg.line[i];
            int idx = ascii < 97 ? (ascii + 32 - 97) : (ascii - 97);

            if (idx < 0 || idx >= 26)
                continue;

            ++cmsg.stats[idx];
        }

        cmsg.mtype = rmsg.id;

        if (msgsnd(msgqid, &cmsg, cmsgSz, 0) == -1)
            printf("failed to send counter\n");
    }
}

pid_t startCounter(int msgqid) {
    pid_t child = fork();

    if (child == -1)
        return -1;
    else if (child > 0)
        return child;

    counter(msgqid);
    exit(0);
}

void printStats(int stats[], int id) {
    char out[1024] = {0};
    char buf[20] = {0};

    for (int i = 0; i < 26; ++i) {
        snprintf(buf, sizeof(buf), "%c:%d ", i + 97, stats[i]);

        strncat(out, buf, strlen(buf));
    }

    printf("file-%d: %s\n", id - 1, out);
}

int main(int argc, char *argv[]) {
    size_t rmsgSz = sizeof(rmsg_t) - sizeof(long);
    size_t cmsgSz = sizeof(cmsg_t) - sizeof(long);
    int isChild = 0;
    int cmsgqid, pmsgqid;
    pid_t counter;
    int readersC;
    rmsg_t rmsg;
    cmsg_t cmsg;
    int i;

    if (argc < 2) {
        printf("usage: alphabet-stats-again <file-1> <file-2> ... <file-n>\n");
        return 1;
    }

    readersC = argc - 1;

    cmsgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (cmsgqid == -1) {
        printf("failed to create c msg que\n");
        return 1;
    }

    pmsgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (pmsgqid == -1) {
        printf("failed to create p msg que\n");
        msgctl(cmsgqid, IPC_RMID, NULL);
        return 1;
    }

    counter = startCounter(cmsgqid);
    if (counter == -1) {
        printf("failed to start counter\n");
        msgctl(cmsgqid, IPC_RMID, NULL);
        msgctl(pmsgqid, IPC_RMID, NULL);
        return 1;
    }

    for (i = 1; i < argc; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (child == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        r(cmsgqid, pmsgqid, i + 1, argv[i]);
        return 0;
    }

    while (1) {
        if (msgrcv(pmsgqid, &cmsg, cmsgSz, MSG_READER_TYPE, 0) == -1) {
            printf("failed to rcv parent\n");
            continue;
        }

        printStats(cmsg.stats, cmsg.id);
        --readersC;

        if (readersC == 0)
            break;
    }

    rmsg.mtype = MSG_COUNTER_TYPE;
    rmsg.id = -1;

    if (msgsnd(cmsgqid, &rmsg, rmsgSz, 0) == -1)
        printf("failed to send stop counter\n");

    waitpid(counter, NULL, 0);
    msgctl(cmsgqid, IPC_RMID, NULL);
    msgctl(pmsgqid, IPC_RMID, NULL);
    return 0;
}
