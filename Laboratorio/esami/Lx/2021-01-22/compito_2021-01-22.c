#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define MAX_LEN 1024
#define LINE_READY_SEM 0
#define NEXT_LINE_SEM 1

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    char line[MAX_LEN];
    int turn;
} shmd_t;

void applyFilter(const char *filt, shmd_t *shmd) {
    switch (filt[0]) {
        case '^':
        case '_': {
            const char *tok = filt + 1;
            int slen = strlen(tok);
            char *str;

            while (1) {
                str = strstr(shmd->line, tok);

                if (str == NULL)
                    break;

                for (int i = 0; i < slen; ++i) {
                    if (filt[0] == '^')
                        str[i] = toupper(str[i]);
                    else
                        str[i] = tolower(str[i]);
                }
            }
        }
            break;
        case '%': {
            const char *tok = filt + 1;
            char tmp[MAX_LEN] = {0};
            char tmp2[MAX_LEN] = {0};
            char find[20] = {0};
            char repl[20] = {0};
            char *str;

            sscanf(tok, "%[^,],%[^\n]", find, repl);

            while (1) {
                char bak;
                int len;

                str = strcasestr(shmd->line, find);
                if (str == NULL)
                    break;

                bak = str[0];
                str[0] = '\0'; // tronca temporaneamente la linea per ottenere la parte 1 prima di find
                len = strlen(shmd->line);

                strncpy(tmp, shmd->line, len);

                str[0] = bak; // ripristina la linea

                snprintf(tmp2, sizeof(tmp2), "%s%s%s", tmp, repl,
                         shmd->line + len + strlen(find)); // crea la nuova linea
                snprintf(shmd->line, sizeof(shmd->line), "%s", tmp2);
            }
        }
            break;
        default:
            break;
    }
}

// non sono sicuro sia corretto ma sembra funzionare il 100% delle volte e senza problemi
void filter(int semid, int shmid, const char *filter, int id, int filtersC) {
    struct sembuf sbuf = {0};
    shmd_t *shmd;

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1) {
        printf("failed to attach mem p\n");
        return;
    }

    while (1) {
        SEMAOP(semid, LINE_READY_SEM, sbuf, -1)

        if (shmd->turn == -1) {
            break;

        } else if (shmd->turn != id) { // la parte che più dubbio, può causare inf loop o deadlock in qualche caso raro?
            SEMAOP(semid, LINE_READY_SEM, sbuf, 1)
            continue;
        }

        applyFilter(filter, shmd);
        ++shmd->turn;

        if (shmd->turn <= filtersC) SEMAOP(semid, LINE_READY_SEM, sbuf, 1)

        SEMAOP(semid, NEXT_LINE_SEM, sbuf, 1)
    }
}

void p(char *argv[], int semid, int shmid, int filtersC) {
    struct sembuf sbuf = {0};
    FILE *fd = fopen(argv[1], "r");
    shmd_t *shmd;

    if (fd == NULL) {
        printf("failed to open file\n");
        return;
    }

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1) {
        printf("failed to attach mem p\n");
        fclose(fd);
        return;
    }

    while (fgets(shmd->line, sizeof(shmd->line), fd) != NULL) {
        shmd->turn = 0;

        SEMAOP(semid, LINE_READY_SEM, sbuf, 1)
        SEMAOP(semid, NEXT_LINE_SEM, sbuf, -(filtersC))
        printf("%s", shmd->line);
    }

    fclose(fd);

    shmd->turn = -1;

    SEMAOP(semid, LINE_READY_SEM, sbuf, filtersC)
}

int main(int argc, char *argv[]) {
    int filtersC = argc - 2;
    int isChild = 0;
    union semun sarg;
    int semid, shmid;
    int i;

    if (argc < 3) {
        printf("filter <file.txt> <filter-1> [filter-2] [...]\n");
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shmem\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 2, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, LINE_READY_SEM, SETVAL, sarg);
    semctl(semid, NEXT_LINE_SEM, SETVAL, sarg);

    for (i = 0; i < filtersC; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (child == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        filter(semid, shmid, argv[i + 2], i, filtersC - 1);
        return 0;
    }

    p(argv, semid, shmid, filtersC);
    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
