#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>

#define LINE_READY_SEM 0
#define LINE_PARSED_SEM 1
#define WRITE_SHM_SEM 2

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    char line[2048];
    int matchCount;
} shmd_t;

void r(int shmid, int pipefd, int semid, const char *file, int wordsC) {
    struct sembuf sbuf = {0};
    FILE *fd = fopen(file, "r");
    shmd_t *shmData;

    shmData = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmData == (void *) -1) {
        printf("failed to attach mem r\n");
        fclose(fd);
        return;
    }

    while (fgets(shmData->line, sizeof(shmData->line), fd) != NULL) {
        size_t slen = strlen(shmData->line);

        shmData->matchCount = 0;

        if (shmData->line[slen - 1] == '\n')
            shmData->line[slen - 1] = '\0';

        SEMAOP(semid, LINE_READY_SEM, sbuf, wordsC) // signal line is in shared mem
        SEMAOP(semid, LINE_PARSED_SEM, sbuf, -(wordsC)) // wait for all w processes

        if (shmData->matchCount == wordsC) {
            ssize_t ret = write(pipefd, shmData->line, strlen(shmData->line));

            if (ret == -1)
                printf("failed to write pipe\n");
        }
    }

    strncpy(shmData->line, "qq", strlen("qq"));
    SEMAOP(semid, LINE_READY_SEM, sbuf, wordsC)

    fclose(fd);
}

void w(int shmid, int semid, const char *word) {
    struct sembuf sbuf = {0};
    shmd_t *shmData;

    shmData = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmData == (void *) -1) {
        printf("failed to attach mem r\n");
        return;
    }

    while (1) {
        SEMAOP(semid, LINE_READY_SEM, sbuf, -1)

        if (strncmp(shmData->line, "qq", strlen("qq")) == 0)
            break;

        if (strcasestr(shmData->line, word) != NULL) {
            SEMAOP(semid, WRITE_SHM_SEM, sbuf, -1)
            ++shmData->matchCount;
            SEMAOP(semid, WRITE_SHM_SEM, sbuf, 1)
        }

        SEMAOP(semid, LINE_PARSED_SEM, sbuf, 1)
    }
}

void o(int pipefd) {
    char line[2048] = {0};

    while (read(pipefd, line, sizeof(line)) > 0) { // wait for data and print when received
        printf("%s\n\n", line);
        memset(line, 0, sizeof(line));
    }
}

pid_t startR(int shmid, int pipefd[], int semid, const char *file, int wordsC) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    close(pipefd[0]);
    r(shmid, pipefd[1], semid, file, wordsC);
    exit(0);
}

pid_t startO(int pipefd[]) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    close(pipefd[1]);
    o(pipefd[0]);
    exit(0);
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    union semun sarg;
    pid_t *children;
    int semid, shmid;
    int pipefd[2];
    int childrenC;
    int i;

    if (argc < 3) {
        printf("usage: search-all-words <text-file> <word-1> [<word-2>] ... [<word-n>]\n");
        return 1;
    }

    childrenC = argc - 2;
    children = (pid_t *) malloc(sizeof(pid_t) * childrenC);

    if (children == NULL) {
        printf("failed to alloc children\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 3, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sem\n");
        free(children);
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        semctl(semid, 0, IPC_RMID);
        free(children);
        return 1;
    }

    if (pipe(pipefd) == -1) {
        printf("failed to create pipe\n");
        semctl(semid, 0, IPC_RMID);
        shmctl(shmid, IPC_RMID, NULL);
        free(children);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, LINE_READY_SEM, SETVAL, sarg);
    semctl(semid, LINE_PARSED_SEM, SETVAL, sarg);

    sarg.val = 1;
    semctl(semid, WRITE_SHM_SEM, SETVAL, sarg);

    children[0] = startR(shmid, pipefd, semid, argv[1], childrenC);
    if (children[0] == -1) {
        printf("failed to start r\n");
        semctl(semid, 0, IPC_RMID);
        shmctl(shmid, IPC_RMID, NULL);
        close(pipefd[0]);
        close(pipefd[1]);
        free(children);
        return 1;
    }

    children[1] = startO(pipefd);
    if (children[1] == -1) {
        printf("failed to start o\n");
        semctl(semid, 0, IPC_RMID);
        shmctl(shmid, IPC_RMID, NULL);
        close(pipefd[0]);
        close(pipefd[1]);
        free(children);
        return 1;
    }

    for (i = 2; i < argc; ++i) {
        children[i] = fork();

        if (children[i] == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (children[i] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        w(shmid, semid, argv[i]);
        return 0;
    }

    close(pipefd[0]);
    close(pipefd[1]);

    for (int i = 0; i < childrenC; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    semctl(semid, 0, IPC_RMID);
    shmctl(shmid, IPC_RMID, NULL);
    free(children);
    return 0;
}
