// this is a more complex example, can be simplified for the exam: no list just 1 slot
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/sem.h>

#define LIST_SZ 15 // size of the circular queue in shared memory used by counter and readers to communicate (readerData_t.list)
#define LIST_FULL_SLOTS_SEM 1
#define LIST_FREE_SLOTS_SEM 2

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    char line[1024];
    int lineNo;
    int id;
} readerLine_t;

typedef struct {
    readerLine_t list[LIST_SZ];
    struct sembuf semOpMuxLock;
    struct sembuf semOpMuxULock;
    int listSize; // filled slots count in list
    int writeIdx; // next slot to write to (readers)
    int readIdx; // next slot to read from (counter)
    int semid;
    int readerDoneC;
} readerData_t;

typedef struct {
    long mtype; // required
    int stats[26];
    int lineNo;
    int readerID;
    int counterDone;
} msgData_t;

void initReaderData(readerData_t *rd, int semid) { // init data in shared mem
    rd->semOpMuxLock.sem_num = 0;
    rd->semOpMuxLock.sem_op = -1;
    rd->semOpMuxLock.sem_flg = 0;
    rd->semOpMuxULock.sem_num = 0;
    rd->semOpMuxULock.sem_op = 1;
    rd->semOpMuxULock.sem_flg = 0;
    rd->listSize = 0;
    rd->writeIdx = 0;
    rd->readIdx = 0;
    rd->semid = semid;
    rd->readerDoneC = 0;

    semctl(semid, 0, SETVAL, 1);
}

void readerDataLock(readerData_t *rd) {
    semop(rd->semid, &rd->semOpMuxLock, 1);
}

void readerDataUnlock(readerData_t *rd) {
    semop(rd->semid, &rd->semOpMuxULock, 1);
}

void setReaderAsDone(readerData_t *rd) {
    readerDataLock(rd);
    ++rd->readerDoneC;
    readerDataUnlock(rd);
}

void addToList(readerData_t *rd, const char *itm, int lineNo, int readerID) {
    readerDataLock(rd);

    readerLine_t *rl = &rd->list[rd->writeIdx];

    rl->id = readerID;
    rl->lineNo = lineNo;
    snprintf(rl->line, sizeof(rd->list[0].line), "%s", itm);
    rd->writeIdx = (rd->writeIdx + 1) % LIST_SZ;
    ++rd->listSize;

    readerDataUnlock(rd);
}

readerLine_t getFromList(readerData_t *rd) {
    readerDataLock(rd);

    readerLine_t itm = rd->list[rd->readIdx];

    rd->readIdx = (rd->readIdx + 1) % LIST_SZ;
    --rd->listSize;

    readerDataUnlock(rd);
    return itm;
}

void reader(int readerID, const char *path, int shmid, int semid) {
    FILE *fd = fopen(path, "r");
    struct sembuf sbuf = {0};
    int lineNo = 1;
    readerData_t *rd;
    char line[1024];

    if (fd == NULL) {
        printf("reader %d: unable to read %s\n", readerID, path);
        return;
    }

    rd = (readerData_t *) shmat(shmid, NULL, 0);
    if (rd == (void *) -1) {
        printf("reader %d: failed to attach shared mem\n", readerID);
        fclose(fd);
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        printf("[R%d] riga-%d: %s", readerID, lineNo++, line);

        SEMAOP(semid, LIST_FREE_SLOTS_SEM, sbuf, -1)
        addToList(rd, line, lineNo - 1, readerID);
        SEMAOP(semid, LIST_FULL_SLOTS_SEM, sbuf, 1)
    }

    setReaderAsDone(rd);
    fclose(fd);
}

void counter(int shmid, int semid, int msgqid, int readerC) {
    readerData_t *rd = (readerData_t *) shmat(shmid, NULL, 0);
    size_t msgSize = sizeof(msgData_t) - sizeof(long);
    msgData_t msgData = {.mtype = 1};
    struct sembuf sbuf = {0};
    int lineStats[26] = {0}; // each index is a letter, 0 = a
    int hasWork;

    if (rd == (void *) -1) { // deadlock, unhandled
        printf("counter: failed to attach shared mem\n");
        return;
    }

    initReaderData(rd, semid);
    SEMAOP(semid, LIST_FREE_SLOTS_SEM, sbuf, LIST_SZ)

    hasWork = rd->readerDoneC < readerC || rd->listSize > 0;
    while (hasWork) {
        readerLine_t rl;

        SEMAOP(semid, LIST_FULL_SLOTS_SEM, sbuf, -1)
        rl = getFromList(rd);
        SEMAOP(semid, LIST_FREE_SLOTS_SEM, sbuf, 1)

        // convert each char to a int array index and increment the value
        for (int i = 0; rl.line[i] != '\0'; ++i) {
            int ascii = (int) rl.line[i];
            int statsIdx = ascii < 97 ? (ascii + 32 - 97) : (ascii - 97); // get lowercase char and convert to a index

            if (statsIdx < 0 || statsIdx >= 26)
                continue;

            ++lineStats[statsIdx];
        }

        printf("[C] analizzata riga-%d per R%d\n", rl.lineNo, rl.id);

        hasWork = rd->readerDoneC < readerC || rd->listSize > 0;
        msgData.lineNo = rl.lineNo;
        msgData.readerID = rl.id;
        msgData.counterDone = !hasWork;

        memcpy(&msgData.stats, &lineStats, sizeof(lineStats));
        memset(lineStats, 0, sizeof(lineStats));

        if (msgsnd(msgqid, &msgData, msgSize, 0) == -1)
            printf("counter: failed to send message for reader %d, line %d\n", rl.id, rl.lineNo);
    }
}

pid_t startCounter(int shmid, int semid, int msgqid, int readerC) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    counter(shmid, semid, msgqid, readerC);
    exit(0);
}

void printLineStats(int statsAr[], int lineNo, int readerID) {
    char buf[200] = {0};

    snprintf(buf, sizeof(buf), "[P] statistica della riga-%d di R%d:", lineNo, readerID);

    for (int i = 0; i < 26; ++i) {
        if (statsAr[i] <= 0)
            continue;

        size_t bufJump = strlen(buf);

        snprintf(buf + bufJump, sizeof(buf) - bufJump, " %c:%d", i + 97, statsAr[i]);
    }

    printf("%s\n", buf);
}

void printGlobalStats(int **globStats, int sz) {
    char buf[200] = {0};

    for (int i = 0; i < sz; ++i) {
        snprintf(buf, sizeof(buf), "[P] statistiche finali su %d righe analizzate per R%d:", globStats[i][26], i + 1);

        for (int j = 0; j < 26; ++j) {
            if (globStats[i][j] <= 0)
                continue;

            size_t bufJump = strlen(buf);

            snprintf(buf + bufJump, sizeof(buf) - bufJump, " %c:%d", j + 97, globStats[i][j]);
        }

        printf("%s\n", buf);
    }
}

int **initGlobStatsArray(int sz) {
    int **ret;

    ret = (int **) malloc(sizeof(int *) * sz);
    if (ret == NULL)
        return NULL;

    for (int i = 0; i < sz; ++i) {
        ret[i] = (int *) malloc(sizeof(int) * 27);

        if (ret[i] == NULL) { // undo previous malloc, if any
            for (int j = (i - 1); j >= 0; --j)
                free(ret[j]);

            free(ret);
            return NULL;
        }
    }

    return ret;
}

void freeGlobStatsArray(int **arr, int sz) {
    if (arr == NULL)
        return;

    for (int i = 0; i < sz; ++i)
        free(arr[i]);

    free(arr);
}

void updateGlobStats(int **arr, int readerID, int lineNo, int stats[]) {
    int ridx = readerID - 1; // readerID starts from 1

    for (int i = 0; i < 26; ++i)
        arr[ridx][i] += stats[i];

    if (arr[ridx][26] < lineNo) // update lines count
        arr[ridx][26] = lineNo;
}

int main(int argc, char *argv[]) {
    size_t msgSize = sizeof(msgData_t) - sizeof(long);
    int rprocC = argc - 1;
    int isChild = 0;
    union semun semarg;
    msgData_t msgData;
    pid_t *children;
    int **globStats;
    int shmid;
    int msgqid;
    int semid;
    int i;

    if (argc < 2) {
        printf("Usage: yaas <file-1> <file-2> .. <file-n>\n\n");
        return 1;
    }

    globStats = initGlobStatsArray(rprocC);
    if (globStats == NULL) {
        printf("failed to alloc globStat array\n");
        return 1;
    }

    children = (pid_t *) malloc(sizeof(pid_t) * argc);
    if (children == NULL) {
        printf("failed to alloc children array\n");
        freeGlobStatsArray(globStats, rprocC);
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(readerData_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        free(children);
        freeGlobStatsArray(globStats, rprocC);
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msg que\n");
        free(children);
        freeGlobStatsArray(globStats, rprocC);
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    semid = semget(IPC_PRIVATE, 4, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create semaphores\n");
        free(children);
        freeGlobStatsArray(globStats, rprocC);
        shmctl(shmid, IPC_RMID, NULL);
        msgctl(msgqid, IPC_RMID, NULL);
        return 1;
    }

    semarg.val = 0;
    semctl(semid, LIST_FREE_SLOTS_SEM, SETVAL, semarg);
    semctl(semid, LIST_FULL_SLOTS_SEM, SETVAL, semarg);

    children[0] = startCounter(shmid, semid, msgqid, argc - 1);
    if (children[0] == -1) {
        printf("failed to start counter process\n");
        free(children);
        freeGlobStatsArray(globStats, rprocC);
        shmctl(shmid, IPC_RMID, NULL);
        msgctl(msgqid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        return 1;
    }

    for (i = 1; i < argc; ++i) {
        children[i] = fork();

        if (children[i] == 0) {
            isChild = 1; // this is personal preference, just wanted to remove the loop from stack
            break;

        } else if (children[i] == -1) {
            printf("failed to spawn child %d\n", i);
        }
    }

    if (isChild) {
        reader(i, argv[i], shmid, semid);
        return 0;
    }

    do {
        if (msgrcv(msgqid, &msgData, msgSize, 1, 0) == -1) {
            printf("msgrcv: message error\n");
            continue;
        }

        updateGlobStats(globStats, msgData.readerID, msgData.lineNo, msgData.stats);
        printLineStats(msgData.stats, msgData.lineNo, msgData.readerID);
    } while (!msgData.counterDone);

    printGlobalStats(globStats, rprocC);

    free(children);
    freeGlobStatsArray(globStats, rprocC);
    shmctl(shmid, IPC_RMID, NULL);
    msgctl(msgqid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
