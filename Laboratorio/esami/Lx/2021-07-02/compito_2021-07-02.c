#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define MAX_DESC_LEN 250
#define CAN_BID_SEM 0
#define BID_READY_SEM 1

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    char desc[MAX_DESC_LEN];
    int min;
    int max;
    int current;
    int auctionNo;
    int id;
} shmd_t;

void b(int shmid, int semid, int id) {
    struct sembuf sbuf = {0};
    shmd_t *shmd;

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1) {
        printf("failed to attach mem b %d\n", id);
        return;
    }

    while (1) {
        SEMAOP(semid, CAN_BID_SEM, sbuf, -1)

        if (shmd->auctionNo == -1)
            break;

        shmd->current =
                rand() % (shmd->max + 100); // genera da 0 a max+100, così da avere un risultato ok per il compito
        shmd->id = id;

        printf("B%d: invio offerta di %d EUR per asta n.%d\n", id, shmd->current, shmd->auctionNo);
        SEMAOP(semid, BID_READY_SEM, sbuf, 1)
    }
}

void parseLine(const char *line, shmd_t *shmd) {
    char desc[MAX_DESC_LEN] = {0};
    char min[10] = {0};
    char max[10] = {0};

    sscanf(line, "%[^,],%[^,],%[^\n]", desc, min, max);
    snprintf(shmd->desc, sizeof(shmd->desc), "%s", desc);

    shmd->min = atoi(min);
    shmd->max = atoi(max);
}

void j(int shmid, int semid, const char *file, int biddersC) {
    struct sembuf sbuf = {0};
    FILE *fd = fopen(file, "r");
    char line[100] = {0};
    int totEur = 0;
    int succC = 0;
    int failC = 0;
    int i = 1;
    shmd_t *shmd;

    if (fd == NULL) {
        printf("failed to open file\n");
        return;
    }

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1) {
        printf("failed to attach mem\n");
        fclose(fd);
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        int validC = 0;
        int winEur = 0;
        int bc = 0;
        int winID;

        parseLine(line, shmd);
        printf("J: lancio asta n.%d per %s con offerta minima di %d EUR e massima di %d EUR\n", i, shmd->desc,
               shmd->min, shmd->max);

        shmd->auctionNo = i++;

        SEMAOP(semid, CAN_BID_SEM, sbuf, 1)

        while (bc < biddersC) {
            int isValid;

            SEMAOP(semid, BID_READY_SEM, sbuf, -1)

            isValid = shmd->current <= shmd->max && shmd->current >= shmd->min;
            if (isValid && shmd->current > winEur) {
                winEur = shmd->current;
                winID = shmd->id;
            }

            printf("J: ricevuta offerta da B%d\n", shmd->id);

            if (bc + 1 < biddersC) // se è la fine dell'asta, mantieni B bloccati fino a prossima asta
            SEMAOP(semid, CAN_BID_SEM, sbuf, 1)

            validC += isValid;
            ++bc;
        }

        totEur += winEur;

        if (winEur > 0) {
            printf("J: l'asta n.%d per %s si è conclusa con %d offerte valide su %d; il vincitore è B%d che si aggiudica l'oggetto per %d EUR\n\n",
                   shmd->auctionNo, shmd->desc, validC, biddersC, winID, winEur);

            ++succC;

        } else {
            printf("J: l'asta n.%d per %s si è conclusa senza alcuna offerta valida pertanto l'oggetto non risulta assegnato\n\n",
                   shmd->auctionNo, shmd->desc);

            ++failC;
        }
    }

    shmd->auctionNo = -1;

    fclose(fd);
    printf("J: sono state svolte %d aste di cui %d andate assegnate e %d andate a vuoto; il totale raccolto è di %d EUR\n",
           i - 1, succC, failC, totEur);
    SEMAOP(semid, CAN_BID_SEM, sbuf, biddersC)
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    union semun sarg;
    int shmid, semid;
    int biddersC;
    int i;

    if (argc < 3) {
        printf("auction-house <auction-file> <num-bidders>\n");
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shmem\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 2, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, CAN_BID_SEM, SETVAL, sarg);
    semctl(semid, BID_READY_SEM, SETVAL, sarg);

    biddersC = atoi(argv[2]);

    for (i = 0; i < biddersC; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", child);

        } else if (child == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        srand(time(NULL));
        b(shmid, semid, i + 1);
        return 0;
    }

    j(shmid, semid, argv[1], biddersC);
    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
