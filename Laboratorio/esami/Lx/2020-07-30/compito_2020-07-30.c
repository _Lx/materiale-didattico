#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define MAX_LEN 64
#define LINE_READY_SEM 0
#define LINE_READ_SEM 1

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    char str[MAX_LEN];
} shmd_t;

void r(int shmid, int semid, const char *inf) {
    FILE *fd = inf == NULL ? stdin : fopen(inf, "r");
    struct sembuf sbuf = {0};
    int isStd = inf == NULL;
    char buf[MAX_LEN] = {0};
    shmd_t *shmd;

    if (fd == NULL) {
        printf("failed to open file r\n");
        return;
    }

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1) {
        printf("failed to attach shm r\n");

        if (!isStd)
            fclose(fd);

        return;
    }

    while (fgets(buf, sizeof(buf), fd) != NULL) {
        size_t slen = strlen(buf);

        if (buf[slen - 1] == '\n')
            buf[slen - 1] = '\0';

        if (strncmp(buf, "qq", strlen("qq")) == 0)
            break;

        SEMAOP(semid, LINE_READ_SEM, sbuf, -1)
        snprintf(shmd->str, sizeof(shmd->str), "%s", buf);
        SEMAOP(semid, LINE_READY_SEM, sbuf, 1)
    }

    if (!isStd)
        fclose(fd);

    SEMAOP(semid, LINE_READ_SEM, sbuf, -1)
    snprintf(shmd->str, sizeof(shmd->str), "qq");
    SEMAOP(semid, LINE_READY_SEM, sbuf, 1)
}

void w(int shmid, int semid, const char *outf) {
    FILE *fd = outf == NULL ? stdout : fopen(outf, "w");
    struct sembuf sbuf = {0};
    int isStd = outf == NULL;
    shmd_t *shmd;

    if (fd == NULL) {
        printf("failed to open file r\n");
        return;
    }

    shmd = (shmd_t *) shmat(shmid, NULL, SHM_RDONLY);
    if (shmd == (void *) -1) {
        printf("failed to attach shm r\n");

        if (!isStd)
            fclose(fd);

        return;
    }

    while (1) {
        SEMAOP(semid, LINE_READY_SEM, sbuf, -2)

        if (strncmp(shmd->str, "qq", strlen("qq")) == 0)
            break;

        fprintf(fd, "%s\n", shmd->str);
        SEMAOP(semid, LINE_READ_SEM, sbuf, 1)
    }

    if (!isStd)
        fclose(fd);

    SEMAOP(semid, LINE_READ_SEM, sbuf, 1)
}

pid_t startProc(int type, int shmid, int semid, const char *inf, const char *outf) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    if (type == 0)
        r(shmid, semid, inf);
    else
        w(shmid, semid, outf);

    exit(0);
}

int isPalindrome(const char *str) {
    char buf[MAX_LEN] = {0};

    for (int i = 0, j = strlen(str) - 1; j >= 0; ++i, --j)
        buf[i] = str[j];

    return strncasecmp(buf, str, strlen(buf)) == 0;
}

int main(int argc, char *argv[]) {
    char *inf = argc > 1 ? argv[1] : NULL;
    char *outf = argc > 2 ? argv[2] : NULL;
    struct sembuf sbuf = {0};
    union semun sarg;
    int shmid, semid;
    shmd_t *shmd;

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 2, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, LINE_READY_SEM, SETVAL, sarg);

    sarg.val = 1;
    semctl(semid, LINE_READ_SEM, SETVAL, sarg);

    if (startProc(0, shmid, semid, inf, outf) == -1) {
        printf("failed to start r\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        return 1;
    }

    if (startProc(1, shmid, semid, inf, outf) == -1) {
        printf("failed to start w\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        return 1;
    }

    shmd = (shmd_t *) shmat(shmid, NULL, SHM_RDONLY);
    if (shmd == (void *) -1) {
        printf("failed to attach shm\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);

        return 1;
    }

    while (1) {
        SEMAOP(semid, LINE_READY_SEM, sbuf, -1)

        if (strncmp(shmd->str, "qq", strlen("qq")) == 0) {
            SEMAOP(semid, LINE_READY_SEM, sbuf, 2)
            break;
        }

        if (!isPalindrome(shmd->str)) {
            SEMAOP(semid, LINE_READ_SEM, sbuf, 1)
            continue;
        }

        SEMAOP(semid, LINE_READY_SEM, sbuf, 2)
    }

    SEMAOP(semid, LINE_READ_SEM, sbuf, -1)
    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
