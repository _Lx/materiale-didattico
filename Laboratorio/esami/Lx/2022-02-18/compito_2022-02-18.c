#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/wait.h>

#define MAX_WORD_LEN 50

typedef struct {
    long mtype;
    char str1[MAX_WORD_LEN];
    char str2[MAX_WORD_LEN];
} strings_t;

typedef struct {
    long mtype;
    int compRet;
} compret_t;

void freeWordsList(char **wlist, int len) {
    for (int i = 0; i < len; ++i)
        free(wlist[i]);

    free(wlist);
}

void sorter(int msgqid, const char *file) {
    size_t strsMsgSz = sizeof(strings_t) - sizeof(long);
    size_t cmpMsgSz = sizeof(compret_t) - sizeof(long);
    char line[MAX_WORD_LEN] = {0};
    FILE *fd = fopen(file, "r");
    strings_t msg = {.mtype = 2};
    int listItmC = 0;
    int listSz = 2;
    char **wordsList;
    compret_t cmpres;
    int ret;

    if (fd == NULL) {
        printf("failed to open file\n");
        return;
    }

    wordsList = (char **) malloc(sizeof(char *) * listSz);
    if (wordsList == NULL) {
        printf("failed to alloc wordslist\n");
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        int slen = strlen(line);

        if (listItmC >= listSz) {
            char **tmp;

            listSz *= 2;
            tmp = (char **) realloc(wordsList, sizeof(char *) * listSz);

            if (tmp == NULL) { // realloc senza memory leak
                printf("failed to realloc wordslist\n");
                freeWordsList(wordsList, listItmC);
                return;
            }

            wordsList = tmp;
        }

        wordsList[listItmC] = (char *) malloc(sizeof(char) * MAX_WORD_LEN);
        if (wordsList[listItmC] == NULL) {
            printf("failed to alloc word\n");
            freeWordsList(wordsList, listItmC);
            return;
        }

        if (line[slen - 1] == '\n')
            line[slen - 1] = '\0';

        strncpy(wordsList[listItmC], line, slen);
        ++listItmC;
    }

    fclose(fd);

    for (int i = 0, l = (listItmC - 1); i < l; ++i) { // bubblesort
        for (int j = 0, l2 = (l - i); j < l2; ++j) {
            snprintf(msg.str1, sizeof(msg.str1), "%s", wordsList[j]);
            snprintf(msg.str2, sizeof(msg.str2), "%s", wordsList[j + 1]);

            if (msgsnd(msgqid, &msg, strsMsgSz, 0) == -1) {
                printf("failed to send compare msg\n");
                continue;
            }

            if (msgrcv(msgqid, &cmpres, cmpMsgSz, 1, 0) == -1) {
                printf("failed to recv compare msg\n");
                continue;
            }

            if (cmpres.compRet > 0) {
                char *tmp = wordsList[j];

                wordsList[j] = wordsList[j + 1];
                wordsList[j + 1] = tmp;
            }
        }
    }

    snprintf(msg.str1, sizeof(msg.str1), "end");

    if (msgsnd(msgqid, &msg, strsMsgSz, 0) == -1)
        printf("failed to send end\n");

    msg.mtype = 3;

    for (int i = 0; i < listItmC; ++i) {
        snprintf(msg.str1, sizeof(msg.str1), "%s", wordsList[i]);

        if (msgsnd(msgqid, &msg, strsMsgSz, 0) == -1)
            printf("failed to send msg to parent\n");
    }

    freeWordsList(wordsList, listItmC);
    snprintf(msg.str1, sizeof(msg.str1), "end");

    if (msgsnd(msgqid, &msg, strsMsgSz, 0) == -1)
        printf("failed to send end 2\n");

}

void comparer(int msgqid) {
    size_t strsMsgSz = sizeof(strings_t) - sizeof(long);
    size_t cmpMsgSz = sizeof(compret_t) - sizeof(long);
    compret_t cmpres = {.mtype = 1};
    strings_t msg;

    while (1) {
        if (msgrcv(msgqid, &msg, strsMsgSz, 2, 0) == -1) {
            printf("failed to recv comp strs\n");
            continue;

        } else if (strncmp(msg.str1, "end", strlen("end")) == 0) {
            break;
        }

        cmpres.compRet = strcasecmp(msg.str1, msg.str2);

        if (msgsnd(msgqid, &cmpres, cmpMsgSz, 0) == -1)
            printf("failed to send comp res\n");
    }
}

pid_t startProc(int type, int msgqid, const char *file) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    if (type == 0)
        sorter(msgqid, file);
    else
        comparer(msgqid);

    exit(0);
}

int main(int argc, char *argv[]) {
    size_t strsMsgSz = sizeof(strings_t) - sizeof(long);
    pid_t children[2];
    strings_t msg;
    int msgqid;

    if (argc < 2) {
        printf("usage: sort-list <file>\n");
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msg queue\n");
        return 1;
    }

    for (int i = 0; i < 2; ++i) {
        children[i] = startProc(i, msgqid, argv[1]);

        if (children[i] == -1) {
            printf("failed to start %s\n", (i == 0) ? "sorter" : "comparer");
            return 1;
        }
    }

    while (1) {
        if (msgrcv(msgqid, &msg, strsMsgSz, 3, 0) == -1) {
            printf("failed to recv out strs\n");
            continue;

        } else if (strncmp(msg.str1, "end", sizeof("end")) == 0) {
            break;
        }

        printf("%s\n", msg.str1);
    }

    msgctl(msgqid, IPC_RMID, NULL);
    return 0;
}
