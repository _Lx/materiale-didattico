#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>

#define FREG_READY_SEM 0
#define DIR_READY_SEM 1
#define DATA_READ_SEM 2

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

// non sono sicuro, ma il testo non è chiaro
// che diamine è un oggetto del file system? in c? suppongo una mia struct..
typedef struct {
    char path[PATH_MAX];
    unsigned char type;
    off_t size;
} fsobj_t;

typedef struct {
    fsobj_t fsobj;
    int stop;
} shmd_t;

void reader(int semid, int shmid, const char *path, int readersC) {
    struct sembuf sbuf = {0};
    DIR *dfd = opendir(path);
    char buf[PATH_MAX] = {0};
    struct dirent *dt;
    struct stat st;
    shmd_t *shmd;

    if (dfd == NULL) {
        printf("failed to open %s\n", path);
        return;
    }

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1) {
        closedir(dfd);
        printf("failed to attach mem %s\n", path);
        return;
    }

    dt = readdir(dfd);
    while (dt != NULL) {
        if ((dt->d_type != DT_REG && dt->d_type != DT_DIR) ||
            (dt->d_type == DT_DIR &&
             (strncmp(dt->d_name, ".", strlen(".")) == 0 || strncmp(dt->d_name, "..", strlen("..")) == 0))) {
            dt = readdir(dfd);
            continue;
        }

        SEMAOP(semid, DATA_READ_SEM, sbuf, -1)
        snprintf(buf, sizeof(buf), "%s/%s", path, dt->d_name);
        stat(buf, &st);

        snprintf(shmd->fsobj.path, sizeof(shmd->fsobj.path), "%s", buf);
        shmd->fsobj.type = dt->d_type;
        shmd->fsobj.size = st.st_size;

        if (dt->d_type == DT_REG) SEMAOP(semid, FREG_READY_SEM, sbuf, 1)
        else SEMAOP(semid, DIR_READY_SEM, sbuf, 1)

        dt = readdir(dfd);
    }

    SEMAOP(semid, DATA_READ_SEM, sbuf, -1)
    ++shmd->stop;

    if (shmd->stop >= readersC) {
        shmd->stop = -1;

        SEMAOP(semid, FREG_READY_SEM, sbuf, 1)
        SEMAOP(semid, DIR_READY_SEM, sbuf, 1)

    } else {
        SEMAOP(semid, DATA_READ_SEM, sbuf, 1)
    }
}

void fconsumer(int semid, int shmid) {
    shmd_t *shmd = (shmd_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};

    if (shmd == (void *) -1) {
        printf("fconsumer failed to attach mem\n");
        return;
    }

    while (1) {
        SEMAOP(semid, FREG_READY_SEM, sbuf, -1)

        if (shmd->stop == -1)
            break;

        printf("%s [file di %ld bytes]\n", shmd->fsobj.path, shmd->fsobj.size);
        SEMAOP(semid, DATA_READ_SEM, sbuf, 1)
    }
}

void dconsumer(int semid, int shmid) {
    shmd_t *shmd = (shmd_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};

    if (shmd == (void *) -1) {
        printf("dconsumer failed to attach mem\n");
        return;
    }

    while (1) {
        SEMAOP(semid, DIR_READY_SEM, sbuf, -1)

        if (shmd->stop == -1)
            break;

        printf("%s [directory]\n", shmd->fsobj.path);
        SEMAOP(semid, DATA_READ_SEM, sbuf, 1)
    }
}

pid_t startProc(int type, int semid, int shmid) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    if (type == 0)
        fconsumer(semid, shmid);
    else
        dconsumer(semid, shmid);

    exit(0);
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    union semun sarg;
    pid_t *children;
    int shmid, semid;
    int i;

    if (argc < 2) {
        printf("usage: list-dirs <dir1> <dir2> <...>\n");
        return 1;
    }

    children = (pid_t *) malloc(sizeof(pid_t) * (argc - 1));
    if (children == NULL) {
        printf("failed to alloc children\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 3, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        free(children);
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        semctl(semid, 0, IPC_RMID);
        free(children);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, FREG_READY_SEM, SETVAL, sarg);
    semctl(semid, DIR_READY_SEM, SETVAL, sarg);

    sarg.val = 1;
    semctl(semid, DATA_READ_SEM, SETVAL, sarg);

    children[0] = startProc(0, semid, shmid);
    if (children[0] == -1) {
        printf("failed to start fconsumer\n");
        free(children);
        semctl(semid, 0, IPC_RMID);
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    children[1] = startProc(1, semid, shmid);
    if (children[1] == -1) {
        printf("failed to start dconsumer\n");
        free(children);
        semctl(semid, 0, IPC_RMID);
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    for (i = 1; i < argc; ++i) {
        int idx = i + 1;

        children[idx] = fork();

        if (children[idx] == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (children[idx] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        size_t slen = strlen(argv[i]);

        if (argv[i][slen - 1] == '/')
            argv[i][slen - 1] = '\0';

        reader(semid, shmid, argv[i], argc - 1);
        return 0;
    }

    for (int i = 0, l = argc - 1; i < l; ++i)
        waitpid(children[i], NULL, 0);

    free(children);
    semctl(semid, 0, IPC_RMID);
    shmctl(shmid, IPC_RMID, NULL);
    return 0;
}
