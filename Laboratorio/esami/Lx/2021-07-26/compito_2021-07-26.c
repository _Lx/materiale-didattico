#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/types.h>

#define PATH_READY_SEM 0
#define PATH_READ_SEM 1

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    long mtype;
    int fsize;
    int scanid;
} msgd_t;

typedef struct {
    char path[PATH_MAX];
    int scanid;
    int scanDoneC;
} shmd_t;

void scanner(int semid, shmd_t *shmd, const char *path, int scanid) {
    int hasSlash = path[strlen(path) - 1] == '/';
    struct sembuf sbuf = {0};
    char nameBuf[PATH_MAX] = {
            0}; // tecnicamente molto sbagliato usare lo stack così e può crashare, ma è solo un compito dell'uni
    DIR *dfd = opendir(path);
    struct dirent *drtb;
    int ret;

    while (1) {
        drtb = readdir(dfd);

        if (drtb == NULL)
            break;

        if (drtb->d_type == DT_DIR) {
            if (strncmp(drtb->d_name, ".", strlen(".")) == 0 || strncmp(drtb->d_name, "..", strlen("..")) == 0)
                continue;

            snprintf(nameBuf, sizeof(nameBuf), "%s%s%s", path, hasSlash ? "" : "/", drtb->d_name);
            scanner(semid, shmd, nameBuf, scanid);

        } else if (drtb->d_type == DT_REG) {
            snprintf(nameBuf, sizeof(nameBuf), "%s%s%s", path, hasSlash ? "" : "/", drtb->d_name);
            SEMAOP(semid, PATH_READ_SEM, sbuf, -1)
            shmd->scanid = scanid;
            snprintf(shmd->path, sizeof(shmd->path), "%s", nameBuf);
            SEMAOP(semid, PATH_READY_SEM, sbuf, 1)
        }
    };

    closedir(dfd);
}

void startScanner(int semid, int shmid, const char *path, int scanid) {
    shmd_t *shmem = (shmd_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};

    if (shmem == (void *) -1) {
        printf("failed to attach sh mem for %d\n", scanid);
        return;
    }

    scanner(semid, shmem, path, scanid);

    SEMAOP(semid, PATH_READ_SEM, sbuf, -1)
    memset(shmem->path, 0, sizeof(shmem->path));
    ++shmem->scanDoneC;
    SEMAOP(semid, PATH_READY_SEM, sbuf, 1)
    return;
}

void starter(int semid, shmd_t *shmd, int msgqid, int pathC) {
    size_t msgSz = sizeof(msgd_t) - sizeof(long);
    struct sembuf sbuf = {0};
    msgd_t msg = {.mtype = 1};
    char buf[PATH_MAX] = {0};
    struct stat sb;

    while (1) {
        int id, doneC;

        SEMAOP(semid, PATH_READY_SEM, sbuf, -1)
        id = shmd->scanid;
        doneC = shmd->scanDoneC;
        snprintf(buf, sizeof(buf), "%s", shmd->path);
        SEMAOP(semid, PATH_READ_SEM, sbuf, 1)

        if (doneC >= pathC)
            break;
        else if (buf[0] == '\0')
            continue;

        if (stat(buf, &sb) == 0) {
            // man 2 stat, man pages arch package 5.13-1, st_blocks: Number of 512B blocks - potrebbe cambiare?
            // du output è in bytes, quindi blocks number * block size / 1024
            msg.fsize = ((intmax_t) sb.st_blocks * 512) / 1024;
            msg.scanid = id;

            if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
                printf("failed to send msg\n");

        } else {
            printf("failed to stat %s\n", buf);
        }
    }

    msg.scanid = -1;
    if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
        printf("failed to send stop msg\n");
}

pid_t startStarter(int semid, int shmid, int msgqid, int pathC) {
    pid_t child = fork();
    shmd_t *shmd;

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    shmd = (shmd_t *) shmat(shmid, NULL, 0);
    if (shmd == (void *) -1)
        printf("failed to attach stater shmem\n");
    else
        starter(semid, shmd, msgqid, pathC);

    exit(0);
}

int main(int argc, char *argv[]) {
    size_t msgSz = sizeof(msgd_t) - sizeof(long);
    int pathC = argc - 1;
    int isChild = 0;
    union semun sarg;
    int semid, shmid;
    int msgqid, i;
    int *stats;
    msgd_t msg;

    if (argc < 2) {
        printf("usage: my-du-s [path-1] [path-2] [...]\n");
        return 1;
    }

    stats = (int *) malloc(sizeof(int) * pathC);
    if (stats == NULL) {
        printf("failed to alloc stats\n");
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        free(stats);
        return 1;
    }

    semid = semget(IPC_PRIVATE, 2, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        shmctl(shmid, IPC_RMID, NULL);
        free(stats);
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msq que\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        free(stats);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, PATH_READY_SEM, SETVAL, sarg);

    sarg.val = 1;
    semctl(semid, PATH_READ_SEM, SETVAL, sarg);

    if (startStarter(semid, shmid, msgqid, pathC) == -1) {
        printf("failed to start starter\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        msgctl(msgqid, IPC_RMID, NULL);
        free(stats);
        return 1;
    }

    for (i = 1; i < argc; ++i) {
        pid_t child = fork();

        if (child == 0) {
            isChild = 1;
            break;

        } else if (child == -1) {
            printf("failed to spawn child %d\n", i);
        }
    }

    if (isChild) {
        startScanner(semid, shmid, argv[i], i);
        return 0;
    }

    memset(stats, 0, sizeof(int) * pathC);

    while (1) {
        if (msgrcv(msgqid, &msg, msgSz, 1, 0) == -1) {
            printf("failed to rcv msg\n");
            continue;
        }

        if (msg.scanid == -1)
            break;

        stats[msg.scanid - 1] += msg.fsize;
    }

    // il risultato non coincide con du -s, ma du -s fa un calcolo leggermente diverso
    // da come chiesto nel compito
    for (int i = 0; i < pathC; ++i)
        printf("%u %s\n", stats[i], argv[i + 1]);

    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    msgctl(msgqid, IPC_RMID, NULL);
    free(stats);
    return 0;
}
