#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <sys/wait.h>

#define LINE_SZ 250

typedef struct {
    long mtype;
    char match[LINE_SZ];
} matchmsg_t;

void r(int pipefd, const char *file) {
    int fd = open(file, O_RDONLY);
    char buf[LINE_SZ] = {0};
    struct stat statBuf;
    size_t fsz;
    char *fmap;

    if (fd == -1) {
        printf("failed to open file\n");
        return;
    }

    if (fstat(fd, &statBuf) == -1) {
        printf("failed to stat file\n");
        close(fd);
        return;
    }

    fsz = statBuf.st_size;
    fmap = (char *) mmap(NULL, fsz, PROT_READ, MAP_PRIVATE, fd, 0);

    close(fd);

    if (fmap == MAP_FAILED) {
        printf("failed to map file\n");
        return;
    }

    for (int i = 0; i < fsz;) {
        int j = 0;

        while (fmap[i] != '\n' && i < LINE_SZ && i < fsz)
            buf[j++] = fmap[i++];

        if (write(pipefd, buf, strlen(buf) + 1) == -1)
            printf("failed to write\n");

        ++i;
        memset(buf, 0, sizeof(buf));
        sleep(1);
    }

    if (munmap(fmap, fsz) == -1)
        printf("failed to unmap mem\n");
}

void w(int msgqid) {
    size_t msgSz = sizeof(matchmsg_t) - sizeof(long);
    matchmsg_t msg;

    while (1) {
        int ret = msgrcv(msgqid, &msg, msgSz, 1, 0);

        if (ret == -1) {
            printf("failed to rcv msg\n");
            continue;
        }

        if (strncmp(msg.match, "qq", sizeof("qq")) == 0)
            break;

        printf("%s\n", msg.match);
    }
}

pid_t startProc(int type, int pipefd[], int msgqid, const char *file) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    if (type == 0) {
        close(pipefd[0]);
        r(pipefd[1], file);

    } else {
        close(pipefd[0]);
        close(pipefd[1]);
        w(msgqid);
    }

    exit(0);
}

int main(int argc, char *argv[]) {
    size_t msgSz = sizeof(matchmsg_t) - sizeof(long);
    matchmsg_t msg = {.mtype = 1};
    char buf[LINE_SZ] = {0};
    pid_t children[2];
    int pipefd[2];
    int msgqid;

    if (argc < 3) {
        printf("usage: another-grep <parola> <file>\n");
        return 1;
    }

    if (pipe(pipefd) == -1) {
        printf("failed to open pipe\n");
        return 1;
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to get msg id\n");
        close(pipefd[0]);
        close(pipefd[1]);
        return 1;
    }

    for (int i = 0; i < 2; ++i) {
        children[i] = startProc(i, pipefd, msgqid, argv[2]);

        if (children[i] == -1)
            printf("failed to spawn child %d\n", i);
    }

    close(pipefd[1]);
    sleep(1);

    while (read(pipefd[0], buf, sizeof(buf)) > 0) {
        if (strcasestr(buf, argv[1]) != NULL) {
            strncpy(msg.match, buf, strlen(buf));

            if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
                printf("failed to send msg\n");
        }

        memset(buf, 0, sizeof(buf));
        sleep(1);
    }

    snprintf(msg.match, sizeof(msg.match), "qq");

    if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
        printf("failed to send stop msg\n");

    for (int i = 0; i < 2; ++i)
        waitpid(children[i], NULL, 0);

    close(pipefd[0]);
    msgctl(msgqid, IPC_RMID, 0);
    return 0;
}
