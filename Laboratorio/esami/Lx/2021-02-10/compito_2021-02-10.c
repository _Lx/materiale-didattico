#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <sys/stat.h>

#define SHM_AL_READY_SEM 0
#define SHM_MZ_READY_SEM 1
#define SHM_STATS_UPDATED_SEM 2

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

void al(int semid, char *shmc, int *shmStats) {
    struct sembuf sbuf = {0};

    while (1) {
        SEMAOP(semid, SHM_AL_READY_SEM, sbuf, -1)

        if (*shmc == '0')
            break;

        ++shmStats[((int) *shmc) - 97];
        SEMAOP(semid, SHM_STATS_UPDATED_SEM, sbuf, 1)
    }

}

void mz(int semid, char *shmc, int *shmStats) {
    struct sembuf sbuf = {0};

    while (1) {
        SEMAOP(semid, SHM_MZ_READY_SEM, sbuf, -1)

        if (*shmc == '0')
            break;

        ++shmStats[((int) *shmc) - 97];
        SEMAOP(semid, SHM_STATS_UPDATED_SEM, sbuf, 1)
    }

}

void counter(int type, int semid, int shmid, int shmid2) {
    int *shmStats;
    char *shmc;

    shmc = (char *) shmat(shmid, NULL, 0);
    if (shmc == (void *) -1) {
        printf("failed to attach mem c %d\n", type);
        return;
    }

    shmStats = (int *) shmat(shmid2, NULL, 0);
    if (shmStats == (void *) -1) {
        printf("failed to attach mem stats\n");
        return;
    }

    if (type == 0)
        al(semid, shmc, shmStats);
    else
        mz(semid, shmc, shmStats);
}

void printStats(int shmid2) {
    char out[200] = {0};
    int *shmStats;

    shmStats = (int *) shmat(shmid2, NULL, 0);
    if (shmStats == (void *) -1) {
        printf("failed to attach mem stats p\n");
        return;
    }

    for (int i = 0; i < 26; ++i) {
        char buf[50] = {0};

        snprintf(buf, sizeof(buf), "%c:%d ", i + 97, shmStats[i]);
        strncat(out, buf, strlen(buf));
    }

    printf("stats: %s\n", out);
}

void p(int semid, int shmid, int shmid2, const char *file) {
    struct sembuf sbuf = {0};
    int fd = open(file, O_RDONLY);
    struct stat sb;
    char *fmap;
    char *shmc;

    if (fd == -1) {
        printf("failed to open file\n");
        return;
    }

    if (stat(file, &sb) == -1) {
        printf("failed to stat file\n");
        close(fd);
        return;
    }

    fmap = (char *) mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (fmap == MAP_FAILED) {
        printf("failed to mmap\n");
        close(fd);
        return;
    }

    close(fd);

    shmc = (char *) shmat(shmid, NULL, 0);
    if (shmc == (void *) -1) {
        printf("failed to attach mem p\n");
        munmap(fmap, sb.st_size);
        return;
    }

    for (int i = 0, l = sb.st_size; i < l; ++i) {
        int c = tolower(fmap[i]);

        if (c < 97 || c > 122)
            continue;

        *shmc = c;

        if (c <= 108) SEMAOP(semid, SHM_AL_READY_SEM, sbuf, 1)
        else SEMAOP(semid, SHM_MZ_READY_SEM, sbuf, 1)

        SEMAOP(semid, SHM_STATS_UPDATED_SEM, sbuf, -1)
    }

    *shmc = '0';
    SEMAOP(semid, SHM_AL_READY_SEM, sbuf, 1)
    SEMAOP(semid, SHM_MZ_READY_SEM, sbuf, 1)
    munmap(fmap, sb.st_size);
    printStats(shmid2);
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    int shmid, shmid2, semid;
    union semun sarg;
    int i;

    if (argc < 2) {
        printf("alpha-stats <file.txt>\n");
        return 1;
    }

    shmid = shmget(IPC_PRIVATE, sizeof(char), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shmem\n");
        return 1;
    }

    shmid2 = shmget(IPC_PRIVATE, sizeof(int) * 26, IPC_CREAT | IPC_EXCL | 0600);
    if (shmid2 == -1) {
        printf("failed to create shmem 2\n");
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    semid = semget(IPC_PRIVATE, 3, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        shmctl(shmid, IPC_RMID, NULL);
        shmctl(shmid2, IPC_RMID, NULL);
        return 1;
    }

    sarg.val = 0;
    semctl(semid, SHM_AL_READY_SEM, SETVAL, sarg);
    semctl(semid, SHM_MZ_READY_SEM, SETVAL, sarg);
    semctl(semid, SHM_STATS_UPDATED_SEM, SETVAL, sarg);

    for (i = 0; i < 2; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (child == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        counter(i, semid, shmid, shmid2);
        return 0;
    }

    p(semid, shmid, shmid2, argv[1]);
    shmctl(shmid, IPC_RMID, NULL);
    shmctl(shmid2, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
