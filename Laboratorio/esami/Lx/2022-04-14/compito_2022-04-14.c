#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/msg.h>
#include <sys/wait.h>

#define QUERYPROC_CNT 2

typedef struct {
    char name[200];
    int val;
} dbentry_t;

typedef struct {
    long mtype;
    char name[200];
    int procID;
    int val;
} query_t;

typedef struct {
    int count;
    int valsum;
} querystats_t;

void populateDbEntry(dbentry_t *entry, const char *str) {
    size_t slen = strlen(str);
    char numStr[20] = {0};
    int divPos = 0;

    for (int i = slen - 1; i >= 0; --i) {
        if (str[i] != ':')
            continue;

        divPos = i;
        break;
    }

    strncpy(numStr, str + divPos + 1, slen - divPos - 2);
    strncpy(entry->name, str, divPos);

    entry->val = atoi(numStr);
}

void inP(int procID, int msgqid, const char *file) {
    size_t msgSize = sizeof(query_t) - sizeof(long);
    query_t queryMsg = {.mtype = 1, .procID = procID};
    FILE *fd = fopen(file, "r");
    char line[200] = {0};
    int i = 1;

    if (fd == NULL) {
        printf("failed to open query file %s\n", file);
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        size_t slen = strlen(line);

        if (line[slen - 1] == '\n')
            line[slen - 1] = '\0';

        snprintf(queryMsg.name, sizeof(queryMsg.name), "%s", line);

        if (msgsnd(msgqid, &queryMsg, msgSize, 0) == -1)
            printf("failed to send query from %d\n", procID);
        else
            printf("IN%d: inviata query n.%d  '%s'\n", procID, i++, line);
    }

    fclose(fd);

    queryMsg.procID = -1;
    if (msgsnd(msgqid, &queryMsg, msgSize, 0) == -1)
        printf("failed to send stop from %d\n", procID);
}

void db(int msgqidC, int msgqidO, const char *dbfile) {
    size_t msgSize = sizeof(query_t) - sizeof(long);
    query_t queryresMsg = {.mtype = 1};
    int queryProcC = QUERYPROC_CNT;
    FILE *fd = fopen(dbfile, "r");
    char line[250] = {0};
    int listItmC = 0;
    int listSz = 2;
    struct msqid_ds msqds;
    dbentry_t *entries;
    query_t queryMsg;
    size_t ret;

    if (fd == NULL) {
        printf("failed to open dbfile\n");
        return;
    }

    entries = (dbentry_t *) malloc(sizeof(dbentry_t) * listSz);
    if (entries == NULL) {
        printf("failed to alloc entries\n");
        fclose(fd);
        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        if (listItmC >= listSz) {
            dbentry_t *tmp;

            listSz *= 2;
            tmp = (dbentry_t *) realloc(entries, sizeof(dbentry_t) * listSz);

            if (tmp == NULL) { // realloc senza memory leak
                printf("failed to realloc db entries\n");
                free(entries);
                fclose(fd);
                return;
            }

            entries = tmp;
        }

        populateDbEntry(&entries[listItmC++], line);
    }

    fclose(fd);
    printf("DB: letti n. %d record da file\n", listItmC);

    do {
        int found = -1;

        ret = msgrcv(msgqidC, &queryMsg, msgSize, 1, 0);

        if (msgctl(msgqidC, IPC_STAT, &msqds) == -1) {
            printf("failed to get message queue stats\n");
            continue;
        }

        if (ret == -1) {
            printf("db message rcv error\n");
            continue;

        } else if (queryMsg.procID == -1) {
            --queryProcC;
            continue;
        }

        for (int i = 0; i < listItmC; ++i) {
            if (strncmp(queryMsg.name, entries[i].name, strlen(entries[i].name)) != 0)
                continue;

            found = i;
            break;
        }

        if (found >= 0) {
            printf("DB: query '%s' da IN%d trovata con valore %d\n", queryMsg.name, queryMsg.procID,
                   entries[found].val);

            queryresMsg.procID = queryMsg.procID;
            queryresMsg.val = entries[found].val;

            strncpy(queryresMsg.name, queryMsg.name, strlen(queryMsg.name));

            ret = msgsnd(msgqidO, &queryresMsg, msgSize, 0);
            if (ret == -1)
                printf("failed to send to out\n");

        } else {
            printf("DB: query '%s' da IN%d non trovata\n", queryMsg.name, queryMsg.procID);
        }
    } while (queryProcC > 0 || msqds.msg_qnum > 0);

    queryresMsg.procID = -1;
    if (msgsnd(msgqidO, &queryresMsg, msgSize, 0) == -1)
        printf("failed to send stop to out\n");

    free(entries);
}

void out(int msgqid) {
    size_t msgSize = sizeof(query_t) - sizeof(long);
    querystats_t stats[QUERYPROC_CNT] = {0};
    query_t qres;

    while (1) {
        if (msgrcv(msgqid, &qres, msgSize, 1, 0) == -1) {
            printf("[O] message rcv error\n");
            continue;

        } else if (qres.procID == -1) {
            break;
        }

        stats[qres.procID - 1].valsum += qres.val;
        ++stats[qres.procID - 1].count;
    }

    for (int i = 0; i < QUERYPROC_CNT; ++i)
        printf("OUT: ricevuti n.%d valori validi per IN%d con totale %d\n", stats[i].count, i + 1, stats[i].valsum);
}

pid_t startDB(int msgqidC, int msgqidO, const char *dbfile) {
    pid_t child = fork();

    if (child == -1)
        return -1;
    else if (child > 0)
        return child;

    db(msgqidC, msgqidO, dbfile);
    exit(0);
}

pid_t startOut(int msgqid) {
    pid_t child = fork();

    if (child == -1)
        return -1;
    else if (child > 0)
        return child;

    out(msgqid);
    exit(0);
}

void freeMsgIDs(int msgqids[]) {
    for (int i = 0; i < 2; ++i)
        msgctl(msgqids[i], IPC_RMID, NULL);
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    pid_t children[4];
    int msgqids[2];
    int i;

    if (argc != 4) {
        printf("usage: lookup-database <db-file> <query-file-1> <query-file-2>\n");
        return 1;
    }

    for (int i = 0; i < 2; ++i) {
        msgqids[i] = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);

        if (msgqids[i] == -1) {
            printf("failed to create message queue\n");

            for (int j = (i - 1); j >= 0; --j) // undo previous msgget
                msgctl(msgqids[j], IPC_RMID, NULL);

            return 1;
        }
    }

    children[0] = startOut(msgqids[1]);
    if (children[0] == -1) {
        printf("failed to start out\n");
        freeMsgIDs(msgqids);
        return 1;
    }

    children[1] = startDB(msgqids[0], msgqids[1], argv[1]);
    if (children[1] == -1) {
        printf("failed to start db\n");
        freeMsgIDs(msgqids);
        return 1;
    }

    for (i = 2; i < 4; ++i) {
        children[i] = fork();

        if (children[i] == -1) {
            printf("failed to spwan child %d\n", i);

        } else if (children[i] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        inP(i - 1, msgqids[0], argv[i]);
        return 0;
    }

    for (int i = 0; i < 4; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    freeMsgIDs(msgqids);
    return 0;
}
