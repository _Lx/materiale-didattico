#include <stdio.h>
#include <sys/wait.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define FIFO_PATH "/tmp/examfifo"
#define MSG_J_TYPE 3

typedef struct {
    long mtype;
    int pID;
    char move;
    int stop;
} msg_t;

char getMove() {
    int m = rand() % 3;

    return m == 0 ? 'c' : (m == 1 ? 's' : 'f');
}

void p(int msgqid, int id) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    msg_t msg;

    while (1) {
        if (msgrcv(msgqid, &msg, msgSz, id, 0) == -1) { // attendi richiesta mossa
            printf("failed to rcv p%d\n", id);
            continue;
        }

        if (msg.stop == 1)
            break;

        msg.mtype = MSG_J_TYPE;
        msg.move = getMove();
        msg.pID = id - 1;

        printf("P%d: mossa '%s'\n", id, msg.move == 'c' ? "carta" : (msg.move == 's' ? "sasso" : "forbice"));

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send p%d\n", id);
    }
}

char getWinn(char m1, char m2) {
    if ((m1 == 's' && m2 == 'c') || (m1 == 'f' && m2 == 's') || (m1 == 'c' && m2 == 'f'))
        return '2';

    return '1';
}

void j(int msgqid, int gnum) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    int fd = open(FIFO_PATH, O_WRONLY);
    char moves[2] = {0};
    msg_t msg;

    if (fd == -1) {
        printf("failed to open fifo\n");
        return;
    }

    for (int i = 0; i < gnum; ++i) {
        printf("G:  inizio partita n.%d\n", i + 1);

        for (int j = 1; j <= 2; ++j) { // invia richiesta mossa
            msg.mtype = j;

            if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
                printf("failed to send msg j\n");
        }

        for (int j = 0; j < 2; ++j) { // ricevi mosse
            if (msgrcv(msgqid, &msg, msgSz, MSG_J_TYPE, 0) == -1)
                printf("failed to rcv j\n");

            moves[msg.pID] = msg.move;
        }

        if (moves[0] == moves[1]) {
            printf("G:  partita n.%d patta e quindi da ripetere\n", i + 1);
            --i;

        } else {
            char winner = getWinn(moves[0], moves[1]);
            char buf[2] = {0};

            printf("G:  partita n.%d vinta da P%c\n", i + 1, winner);
            snprintf(buf, sizeof(buf), "%c", winner);
            write(fd, buf, strlen(buf) + 1);
            usleep(80);
        }
    }

    close(fd);

    msg.stop = 1;

    for (int i = 0; i < 2; ++i) { // invia stop
        msg.mtype = i + 1;

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send stop j\n");
    }
}

void t() {
    int fd = open(FIFO_PATH, O_RDONLY);
    int stats[2] = {0};
    char buf[2] = {0};

    if (fd == -1) {
        printf("failed to open fifo t\n");
        return;
    }

    while (read(fd, buf, 2) > 0) {
        int idx = atoi(buf) - 1;

        ++stats[idx];
        printf("T:  classifica temporanea: P1=%d P2=%d\n", stats[0], stats[1]);
    }

    printf("T: classifica finale: P1=%d P2=%d\n", stats[0], stats[1]);

    if (stats[0] == stats[1])
        printf("T: nessun vincitore del torneo, patta\n");
    else
        printf("T: vincitore del torneo: P%d\n", stats[0] > stats[1] ? 1 : 2);

    close(fd);
}

pid_t startT() {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    t();
    exit(0);
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    pid_t children[3];
    int msgqid;
    int gnum;
    int i;

    if (argc < 2) {
        printf("usage : morra-cinese2 <numero-partite>\n");
        return 0;
    }

    gnum = atoi(argv[1]);

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msg que\n");
        return 1;
    }

    unlink(FIFO_PATH); // elimina file fifo se esiste, se il programma crasha non lo si fa a mano..

    if (mkfifo(FIFO_PATH, 0600) == -1) {
        printf("failed to create fifo\n");
        msgctl(msgqid, IPC_RMID, NULL);
        return 1;
    }

    children[0] = startT();
    if (children[0] == -1) {
        printf("failed to start t\n");
        msgctl(msgqid, IPC_RMID, NULL);
        unlink(FIFO_PATH);
        return 1;
    }

    for (i = 1; i <= 2; ++i) {
        children[i] = fork();

        if (children[i] == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (children[i] == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        srand(time(NULL) * i * 1000);
        p(msgqid, i);
        return 0;
    }

    j(msgqid, gnum);

    for (int i = 0; i < 3; ++i) {
        if (children[i] != -1)
            waitpid(children[i], NULL, 0);
    }

    unlink(FIFO_PATH);
    msgctl(msgqid, IPC_RMID, NULL);
    return 0;
}
