#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define MATCH_START_SEM 0
#define MOVES_SET_SEM 1
#define WINNER_SEM 2

#define SEMAOP(semid, semnum, sbuf, sop) {\
    sbuf.sem_num = semnum; \
    sbuf.sem_op = sop; \
    semop(semid, &sbuf, 1); \
}

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

typedef struct {
    char moveP1;
    char moveP2;
    int winner;
    int stop;
} shmd_t;

char getMove() {
    int rnd = rand() % 3;

    return rnd == 0 ? 'c' : (rnd == 1 ? 's' : 'f');
}

void p(int semid, int shmid, int id) {
    shmd_t *shmd = (shmd_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};
    char mv;

    if (shmd == (void *) -1) {
        printf("failed to attach mem p%d\n", id);
        return;
    }

    while (1) {
        SEMAOP(semid, MATCH_START_SEM, sbuf, -1)

        if (shmd->stop == 1)
            break;

        mv = getMove();

        if (id == 1)
            shmd->moveP1 = mv;
        else
            shmd->moveP2 = mv;

        printf("P%d: mossa '%s'\n", id, mv == 'c' ? "carta" : (mv == 's' ? "sasso" : "forbice"));
        SEMAOP(semid, MOVES_SET_SEM, sbuf, 1)
    }

    SEMAOP(semid, MOVES_SET_SEM, sbuf, 1)
}

int getWinn(shmd_t *shmd) {
    if ((shmd->moveP1 == 'c' && shmd->moveP2 == 'f') || (shmd->moveP1 == 's' && shmd->moveP2 == 'c') ||
        (shmd->moveP1 == 'f' && shmd->moveP2 == 's'))
        return 2;

    return 1;
}

void g(int semid, int shmid, int gnum) {
    shmd_t *shmd = (shmd_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};

    if (shmd == (void *) -1) {
        printf("failed to attach mem g\n");
        return;
    }

    for (int i = 0; i < gnum; ++i) {
        SEMAOP(semid, MOVES_SET_SEM, sbuf, -2)

        if (shmd->moveP1 != shmd->moveP2) {
            shmd->winner = getWinn(shmd);

            printf("G: partita n.%d vinta da P%d\n", i + 1, shmd->winner);
            SEMAOP(semid, WINNER_SEM, sbuf, 1)

        } else {
            printf("G: partita n.%d patta e quindi da ripetere\n", i + 1);
            --i;
            SEMAOP(semid, MATCH_START_SEM, sbuf, 2)
        }
    }

    shmd->stop = 1;
    SEMAOP(semid, WINNER_SEM, sbuf, 1)
    SEMAOP(semid, MATCH_START_SEM, sbuf, 2)

    // pigro, potrei waitpid, ma ormai..
    SEMAOP(semid, WINNER_SEM, sbuf, -1)
    SEMAOP(semid, MOVES_SET_SEM, sbuf, -2)
}

void t(int semid, int shmid) {
    shmd_t *shmd = (shmd_t *) shmat(shmid, NULL, 0);
    struct sembuf sbuf = {0};
    int stats[2] = {0};

    while (1) {
        SEMAOP(semid, WINNER_SEM, sbuf, -1)

        ++stats[shmd->winner - 1];

        if (shmd->stop == 1)
            break;

        printf("T: classifica temporanea: P1=%d P2=%d\n", stats[0], stats[1]);

        SEMAOP(semid, MATCH_START_SEM, sbuf, 2)
    }

    printf("T: classifica finale: P1=%d P2=%d\n", stats[0], stats[1]);
    printf("T: vincitore del torneo: P%d\n", stats[0] > stats[1] ? 1 : 2);
    SEMAOP(semid, WINNER_SEM, sbuf, 1)
}

pid_t startT(int semid, int shmid) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    t(semid, shmid);
    exit(0);
}

int main(int argc, char *argv[]) {
    int isChild = 0;
    union semun sarg;
    int shmid, semid;
    int gnum;
    int i;

    if (argc < 2) {
        printf("usage: morra-cinese <numero-partite>\n");
        return 1;
    }

    gnum = atoi(argv[1]);

    shmid = shmget(IPC_PRIVATE, sizeof(shmd_t), IPC_CREAT | IPC_EXCL | 0600);
    if (shmid == -1) {
        printf("failed to create shared mem\n");
        return 1;
    }

    semid = semget(IPC_PRIVATE, 3, IPC_CREAT | IPC_EXCL | 0600);
    if (semid == -1) {
        printf("failed to create sema\n");
        shmctl(shmid, IPC_RMID, NULL);
        return 1;
    }

    sarg.val = 2;
    semctl(semid, MATCH_START_SEM, SETVAL, sarg);

    sarg.val = 0;
    semctl(semid, MOVES_SET_SEM, SETVAL, sarg);
    semctl(semid, WINNER_SEM, SETVAL, sarg);

    if (startT(semid, shmid) == -1) {
        printf("failed to start t\n");
        shmctl(shmid, IPC_RMID, NULL);
        semctl(semid, 0, IPC_RMID);
        return 1;
    }

    for (i = 1; i <= 2; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (child == 0) {
            isChild = 1;
            break;
        }
    }

    if (isChild) {
        srand(time(NULL) * i * 1000);
        p(semid, shmid, i);
        return 0;
    }

    g(semid, shmid, gnum);

    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    return 0;
}
