Author: Lx

21 esami dal 2022 al 2019 svolti

### note:

il codice è il più corretto possibile ma potrei aver svisto qualcosa

il codice riguardante i semafori è leggermente diverso da quello visto a lezione, ed è corretto, mentre quello a lezione funziona, ma non è effettivamente corretto in alcuni punti.

ad es., dare un int a SETVAL è sbagliato e si basa su miracoli che accadono per vie sconosciute, oggi va, domani...

l'uso della union semun è il corretto unico modo di usare SETVAL.

WAIT e SIGNAL delle lezioni sono state sostituite con 1 macro (SEMAOP) molto più semplice ed efficiente.

#### DA NOTARE CHE QUESTI ESEMPI MANCANO DI MOLTI CHECK, E NON SONO UN ESEMPIO VALIDO DI COME SCRIVERE UN PROGRAMMA FUORI DALL'UNIVERSITA'!!!!

ma vanno bene per i fini dell'esame, e basta!!
