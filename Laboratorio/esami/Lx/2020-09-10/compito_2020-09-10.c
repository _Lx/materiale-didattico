#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

void r(int pipefd, const char *file) {
    int fd = open(file, O_RDONLY);
    char buf[20] = {0};
    struct stat st;
    char *fmap;

    if (stat(file, &st) == -1) {
        printf("failed to stat file\n");
        close(fd);
        return;
    }

    fmap = (char *) mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (fmap == MAP_FAILED) {
        printf("failed to mmap\n");
        close(fd);
        return;
    }

    close(fd);

    for (int i = 0, j = 0, l = st.st_size; i < l; ++i) {
        if (fmap[i] != '\n')
            continue;

        strncpy(buf, fmap + j, i - j);

        if (write(pipefd, buf, strlen(buf) + 1) == -1)
            printf("failed to write pipe\n");

        usleep(200);
        memset(buf, 0, sizeof(buf));
        j = i + 1;
    }

    strncpy(buf, "qq", strlen("qq"));
    if (write(pipefd, buf, strlen("qq") + 1) == -1)
        printf("failed to write stop\n");

    munmap(fmap, st.st_size);
}

void w(int pipefd) {
    char buf[20] = {0};

    while (read(pipefd, buf, sizeof(buf)) > 0) {
        printf("%s\n", buf);
        memset(buf, 0, sizeof(buf));
    }
}

pid_t startProc(int type, int pipefd[], const char *file) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    if (type == 0) {
        close(pipefd[0]);
        r(pipefd[1], file);
        close(pipefd[1]);

    } else {
        close(pipefd[1]);
        w(pipefd[0]);
        close(pipefd[0]);
    }

    exit(0);
}

int isPalindrome(const char *str) { // sono pigro, si può anche senza usare buf
    char buf[20] = {0};

    for (int i = 0, j = strlen(str) - 1; j >= 0; --j, ++i)
        buf[i] = str[j];

    return strncmp(buf, str, strlen(str)) == 0;
}

int main(int argc, char *argv[]) {
    char buf[20] = {0};
    int rpipefd[2];
    int wpipefd[2];

    if (argc < 2) {
        printf("usage: palindrome-filter <input file>\n");
        return 1;
    }

    if (pipe(rpipefd) == -1) {
        printf("failed to open rpipe\n");
        return 1;
    }

    if (pipe(wpipefd) == -1) {
        printf("failed to open wpipe\n");
        close(rpipefd[0]);
        close(rpipefd[1]);
        return 1;
    }

    if (startProc(0, rpipefd, argv[1]) == -1) {
        printf("failed to start r\n");
        close(rpipefd[0]);
        close(rpipefd[1]);
        close(wpipefd[0]);
        close(wpipefd[1]);
        return 1;
    }

    if (startProc(1, wpipefd, argv[1]) == -1) {
        printf("failed to start w\n");
        close(rpipefd[0]);
        close(rpipefd[1]);
        close(wpipefd[0]);
        close(wpipefd[1]);
        return 1;
    }

    close(rpipefd[1]);
    close(wpipefd[0]);

    while (read(rpipefd[0], buf, sizeof(buf)) > 0) {
        if (strncmp(buf, "qq", strlen("qq")) == 0)
            break;
        else if (!isPalindrome(buf))
            continue;

        if (write(wpipefd[1], buf, strlen(buf) + 1) == -1)
            printf("failed to write w\n");

        usleep(100);
        memset(buf, 0, sizeof(buf));
    }

    close(rpipefd[0]);
    close(wpipefd[1]);
    return 0;
}
