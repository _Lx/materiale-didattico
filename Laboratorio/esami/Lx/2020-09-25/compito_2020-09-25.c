#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/msg.h>
#include <string.h>

typedef struct {
    long mtype;
    char line[32];
    int done;
} msg_t;

char *trim(char *str) {
    int len = strlen(str);
    int i = 1;

    if (str[len - 1] == '\n') {
        str[len - 1] = '\0';
        --len;
    }

    while (str[len - i] == ' ')
        str[len - (i++)] = '\0';

    i = 0;
    while (str[i] == ' ')
        ++i;

    return str + i;
}

void r(int msgqid, const char *file) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    msg_t msg = {.mtype = 1};
    FILE *fd = fopen(file, "r");
    char line[32] = {0};

    if (fd == NULL) {
        printf("failed to open file\n");

        msg.done = 1;
        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send stop\n");

        return;
    }

    while (fgets(line, sizeof(line), fd) != NULL) {
        snprintf(msg.line, sizeof(msg.line), "%s", trim(line));

        if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
            printf("failed to send line %s\n", line);
    }

    msg.done = 1;
    if (msgsnd(msgqid, &msg, msgSz, 0) == -1)
        printf("failed to send stop\n");

    fclose(fd);
}

void w(int pipefd) {
    char buf[32] = {0};

    while (read(pipefd, buf, sizeof(buf)) > 0) {
        printf("%s\n", buf);
        memset(buf, 0, sizeof(buf));
    }
}

pid_t startW(int pipefd) {
    pid_t child = fork();

    if (child > 0)
        return child;
    else if (child == -1)
        return -1;

    w(pipefd);
    close(pipefd);
    exit(0);
}

void freeList(char **list) {
    for (int i = 0; i < 200; ++i)
        free(list[i]);

    free(list);
}

int main(int argc, char *argv[]) {
    size_t msgSz = sizeof(msg_t) - sizeof(long);
    int isChild = 0;
    int pipefd[2];
    char **list;
    int msgqid;
    msg_t msg;
    int i, j;
    int ret;

    if (argc != 3) {
        printf("usage: merge-lists <file-1> <file-2>\n");
        return 1;
    }

    list = (char **) malloc(sizeof(char *) * 200);
    if (list == NULL) {
        printf("failed to alloc list\n");
        return 1;
    }

    for (i = 0; i < 200; ++i) {
        list[i] = (char *) malloc(sizeof(char) * 32);

        if (list[i] == NULL) {
            printf("failed to alloc list %d\n", i);

            for (int j = i; j >= 0; --j)
                free(list[j]);

            free(list);
            return 1;
        }

        memset(list[i], 0, sizeof(char) * 32);
    }

    msgqid = msgget(IPC_PRIVATE, IPC_CREAT | IPC_EXCL | 0600);
    if (msgqid == -1) {
        printf("failed to create msg que\n");
        freeList(list);
        return 1;
    }

    ret = pipe(pipefd);
    if (ret == -1) {
        printf("failed to open pipe\n");
        msgctl(msgqid, IPC_RMID, NULL);
        freeList(list);
        return 1;
    }

    if (startW(pipefd[0]) == -1) {
        printf("failed to start w\n");
        close(pipefd[0]);
        msgctl(msgqid, IPC_RMID, NULL);
        freeList(list);
        return 1;
    }

    close(pipefd[0]);

    for (i = 0; i < 2; ++i) {
        pid_t child = fork();

        if (child == -1) {
            printf("failed to spawn child %d\n", i);

        } else if (child == 0) {
            isChild = 1;

            close(pipefd[0]);
            close(pipefd[1]);
            break;
        }
    }

    if (isChild) {
        r(msgqid, argv[i + 1]);
        return 0;
    }

    j = 0;
    while (1) {
        int dupl = 0;

        if (msgrcv(msgqid, &msg, msgSz, 1, 0) == -1) {
            printf("failed to rcv msg\n");
            continue;
        }

        if (msg.done)
            break;

        for (i = 0; i < j; ++i) {
            if (strncasecmp(list[i], msg.line, strlen(msg.line)) != 0)
                continue;

            dupl = 1;
            break;
        }

        if (dupl)
            continue;
        else if (write(pipefd[1], msg.line, strlen(msg.line)) == -1)
            printf("failed to write pipe\n");

        strncpy(list[j], msg.line, strlen(msg.line));
        usleep(80);
        ++j;
    }

    close(pipefd[1]);
    msgctl(msgqid, IPC_RMID, NULL);
    freeList(list);
    return 0;
}
